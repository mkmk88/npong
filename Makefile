TARGET = npong
MAINFILE = main

CC = gcc
CFLAGS = -c -Wall $(shell imlib2-config --cflags) -ggdb -g
OUTDIR = out
NGAMESPATH = lib
NGAMESOUT = lib/bin
NGAMESNAME = libngames
LDFLAGS = -L$(NGAMESPATH)
LIBS_STATIC = -lngames
LIBS = -lpthread \
       -lrt \
       -lpanel \
       -lmenu \
       -lform \
       -lncurses \
       $(shell imlib2-config --libs)
INCDIRS = -Iinc

all: $(TARGET) build-test

$(TARGET): $(MAINFILE)
	$(CC) $(MAINFILE).o -Wl,-Bstatic -L$(NGAMESOUT) $(LIBS_STATIC) -Wl,-Bdynamic $(LIBS) -o $(TARGET)
	mv *.o $(OUTDIR)/

$(MAINFILE): $(NGAMESNAME)
	$(CC) $(MAINFILE).c $(CFLAGS) 

$(NGAMESNAME):
	mkdir -p $(OUTDIR)
	cd $(NGAMESPATH) && make

build-test:
	cd $(NGAMESPATH)/tests && make

test:
	cd $(NGAMESPATH)/tests && ./ngames-test
	
clean:
	rm -rf $(OUTDIR) $(TARGET) && rm -f log.txt
	cd $(NGAMESPATH) && make clean && cd tests && make clean
