# README #

Ngames is a framework which purpose is to create small terminal games based on the [ncurses](https://www.gnu.org/software/ncurses/) library. The idea is to hide all raw ncurses calls from user and provide easy-to-use wrappers needed during game development. Instead of operating on ncurses windows or panels the abstraction layer called "entity" is introduced.

### What is this repository for? ###

* This project contains a [ncurses](https://www.gnu.org/software/ncurses/)-based framework for creating terminal games (see "lib" folder for header file and documentation) and the example pong game. The project is not finished. The single player mode has only a dummy AI and the multiplayer mode is not ready at all. 

### How do I get set up? ###

* Clone this repository.
* Install dependencies (Ubuntu example): sudo apt-get install libimlib2-dev check libncurses5 doxygen
* How to build: run make all
* How to run tests: make test
* How to start: maximize the terminal and run ./npong
* How to quit from gameplay: press "q"
* How to generate documentation: go to "lib" folder and run "doxygen doxygen.config". The "doc" folder will be created. Search for "index.html" file there.

### Who do I talk to? ###

* Michal Kowalczyk kowalczykmichal88+bb@gmail.com
* http://fixbugfix.blogspot.com/