#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <Imlib2.h>
#include "ngames.h"

#define LOGPATH "log.txt"
#define ENTITIES_MAX 128

#define MAX_TERMINAL_WIDTH 157
#define MAX_TERMINAL_HEIGHT 41

/** Maximum number of inputs that can be used in ngames framework.
  */
#define MAX_INPUTS 2

/** Maximum number of listeners attached to one input type.
  */
#define MAX_INPUT_LISTENERS 8

#define NSEC_PER_SECOND 1000000000
#define FRAMES_PER_SECOND 60
#define FRAME_RATE_MSEC (1000 / FRAMES_PER_SECOND)

static FILE *logfile;
static int ng_log_cntr;

static struct misc_data {
    int terminal_width;
    int terminal_height;
} misc_data;

static struct ng_input {
    ng_input_cb_t cb;
    struct ng_entity *listener[MAX_INPUT_LISTENERS];
} inputs[MAX_INPUTS];

/**
 * This is a main placeholder for all entities. It has the 'todo' status.
 * Probably it should be a different data structure (list?). Also, for some
 * reason it should be dynamically allocated, but huh, all this
 * malloc stuff is so much rocket science, isn't it?
 */
static struct ng_entity entity[ENTITIES_MAX];

/** Network related-stuff */
#define NETWORK_BUF_SIZE 256
#define DEFAULT_NETWORK_TIMEOUT 10
static bool is_host;
static bool is_client;
static int lsfd; /** Listening socket file descriptor. */
static int csfd; /** Connected socket file descriptor. */
static int portno;
struct hostent *server;
socklen_t clilen;
struct sockaddr_in serv_addr, cli_addr;
struct timeval network_timeout;
static char network_buffer[NETWORK_BUF_SIZE];

static bool is_time_elapsed(struct timespec *ts, int miliseconds);
static void timespec_diff(struct timespec *start, struct timespec *end, struct timespec *result);

static bool running;

int ng_get_terminal_width()
{
    return misc_data.terminal_width;
}

int ng_get_terminal_height()
{
    return misc_data.terminal_height;
}

static int init_ncurses()
{
    initscr();
    cbreak();
    noecho();
    curs_set(0);
    start_color();
    nodelay(stdscr,TRUE);
    keypad(stdscr, TRUE);

    getmaxyx(stdscr, misc_data.terminal_height, misc_data.terminal_width);
    ng_log("Terminal size: y = %d x = %d\n", misc_data.terminal_height, misc_data.terminal_width);

    if (misc_data.terminal_height < MAX_TERMINAL_HEIGHT || misc_data.terminal_width < MAX_TERMINAL_WIDTH) {
        endwin();
        ng_log("Terminal size is not sufficient. Should be at least %dx%d but it's %dx%d\n",
           MAX_TERMINAL_HEIGHT, MAX_TERMINAL_WIDTH, misc_data.terminal_height, misc_data.terminal_width);
        return -1; 
    }   

    return 0;
}

static void input_keyboard()
{
    int c;
    int i;

    c = getch();

    for (i = 0; i < MAX_INPUT_LISTENERS; i++) {
        if (inputs[I_KEYBOARD].listener[i]) {
            inputs[I_KEYBOARD].listener[i]->input = c;
        }
    }
}

static void input_network()
{
    int c;
    int i;

    if (!is_host && !is_client) {
        return;
    }

    if (is_host) {
        if (ng_rcv_keycode_from_client(&c) == -1) {
            ng_log("Error reading network input\n");
            return;
        }
    } else {
        if (ng_rcv_keycode(&c) == -1) {
            ng_log("Error reading network input\n");
            return;
        }

    }

    for (i = 0; i < MAX_INPUT_LISTENERS; i++) {
        if (inputs[I_NETWORK].listener[i]) {
            inputs[I_NETWORK].listener[i]->input = c;
        }
    }
}

void ng_refresh()
{
    update_panels();
    doupdate();
    refresh();
}

int ng_init()
{
    if (ng_log_init() == -1) {
        printf("Cannot initialize logging. Something is wrong.\n");
        return -1;
    }

    if (init_ncurses() == -1) {
        ng_log("Cannot initialize ncurses.\n");
        return -1;
    }

    if (ng_install_input(I_KEYBOARD, input_keyboard) == -1) {
        ng_log("Cannot install default keyboard input.\n");
        return -1;
    }

    if (ng_install_input(I_NETWORK, input_network) == -1) {
        ng_log("Cannot install default network input.\n");
        return -1;
    }

    running = true;

    return 0;
}

void ng_log(const char *fmt, ...)
{
    va_list args;
    struct timespec ts;

    va_start(args, fmt);
    logfile = fopen(LOGPATH, "a");
    clock_gettime(CLOCK_MONOTONIC, &ts);
    fprintf(logfile, "[%lu.%9lu] ", (unsigned long)ts.tv_sec, (unsigned long)ts.tv_nsec);
    vfprintf(logfile, fmt, args);

    fclose(logfile);
    va_end(args);
}

void ng_log_once(const char *fmt, ...)
{
    va_list args;

    if (ng_log_cntr > 0) {
        return;
    }
    va_start(args, fmt);
    ng_log(fmt, args);
    va_end(args);
    ng_log_cntr += 1;
}

void ng_log_reset_oneshot()
{
    ng_log_cntr = 0;
}

int ng_log_init()
{
    logfile = fopen(LOGPATH, "w");    
    if (logfile == NULL) {
        return -1;
    }
    ng_log("ngames log initialized\n");
    return 0;
}

void ng_log_close()
{
    fclose(logfile);
}

static struct ascii_matrix {
    char ascii_char;
    int treshold;
} ascii_matrix[] = {
    { '@', 0 },
    { '#', 50 },
    { '8', 70 },
    { '&', 100 },
    { 'o', 130 },
    { ':', 160 },
    { '*', 180 },
    { '.', 200 },
    { ' ', 230 }
};

static char get_ascii_char(int value)
{  
    int i;
    int ascii_th_cnt = sizeof(ascii_matrix) / sizeof(struct ascii_matrix);
    
    for (i = 0; i < ascii_th_cnt - 1; i++) {
        if (value >= ascii_matrix[i].treshold &&
            value < ascii_matrix[i + 1].treshold) {
            return ascii_matrix[i].ascii_char;
        }
    }

    return ' ';
}

int ng_image_to_ascii(char *buf, int x, int y, const char *path)
{
   int i, j, k = 0;
   int A, R, G, B, gray;
   DATA32 *data;
   Imlib_Image image, output;

   if (y < 1 || x < 1) {
       ng_log("Convert to ascii failed. Target window is too small y = %d x = %d\n", y, x);
       return -1;
   }
   /* Give little margin */
   y -= 1;
   x -= 1;

   image = imlib_load_image(path);
   imlib_context_set_image(image);
   output = imlib_create_cropped_scaled_image(0, 0, imlib_image_get_width(), imlib_image_get_height(), x, y);
   imlib_free_image();
   imlib_context_set_image(output);
    
    data = imlib_image_get_data_for_reading_only();
    for (i = 1; i < y; i++) {
        for (j = 1; j < x; j++) {              
             A = (int)((int)data[k] & (int)(255 << 24));
             A >>= 24;
             R = (int)((int)data[k] & (int)(255 << 16));
             R >>= 16;
             G = (int)((int)data[k] & (int)(255 << 8));
             G >>= 8;
             B = (int)((int)data[k] & 255);
             gray = (R + G + B) / 3;
             buf[k++] = get_ascii_char(gray);
         }
    }
    imlib_free_image();
    return 0;
}

int ng_fill_window(struct ng_entity *ent, const char c)
{
   int i, j;

   if (!ent) {
       ng_log("Cannot fill window of null entity\n");
       return -1;
   }

   if (!ent->w) {
       ng_log("Entity %s doesn't have a window\n");
       return -1;
   }

   for (i = 1; i < ent->height - 1; i++) {
        for (j = 1; j < ent->width - 1; j++) {
            mvwprintw(ent->w, i, j, "%c", c);
        }
   }

    return 0;
}

static int entity_find_free_slot()
{
    int i;
    for (i = 0; i < ENTITIES_MAX; i++) {
        if (entity[i].type == T_EMPTY) {
            return i;
        }
    }

    return -1;
}

int ng_set_update_cb(struct ng_entity *ent, ng_update_cb_t cb)
{
    if (!ent) {
        ng_log("Cannot set update callback to NULL entity\n");
        return -1;
    }

    ent->update = cb;

    return 0;
}

struct ng_entity *ng_add_entity(ng_entity_type_t type, const char *name)
{
    int i = entity_find_free_slot();
    int j;

    if (i == -1) {
        ng_log("Cannot add more entities. name = %s\n", name);
        return NULL;
    }

    if (name) {
        for (j = 0; j < ENTITIES_MAX; j++) {
            if (!strncmp(entity[j].name, name, NG_MAX_NAME_LENGTH)) {
                ng_log("Cannot create entity. Name already exists: %s\n", name);
                return NULL;
            }
        }
       strncpy(entity[i].name, name, sizeof(entity[i].name));
    } else {
       strncpy(entity[i].name, "default", sizeof(entity[i].name));
    }

    entity[i].type = type;

    ng_set_visible(&entity[i], true);
    ng_set_dir(&entity[i], NG_UNKNOWN_DIR);
    ng_set_speed(&entity[i], 1, 1);
    ng_set_update_cb(&entity[i], NULL);
    ng_set_timer(&entity[i], 0);

    return &entity[i];
}

static int draw_graphics(WINDOW *wdw, struct ng_resource *res)
{
   int i, j, k = 0;

   for (i = 1; i < res->y; i++) {
        for (j = 1; j < res->x; j++, k++) {
            mvwprintw(wdw, i, j, "%c", res->data[k]);
        }
   }

   return 0;
}

int ng_load_image(struct ng_entity *ent, const char *path)
{
   int y, x;
   int A, R, G, B, gray;
   struct stat buf;

   if (!ent) {
       ng_log("Cannot set resource to null entity\n");
       return -1;
   }
   if (!ent->w) {
       ng_log("Cannot set resource to entity that doesn't have a window\n");
       return -1;
   }
   if (!path) {
       ng_log("Cannot set resource. Provided path is incorrect (null).\n");
       return -1;
   }
   if (lstat(path, &buf) == -1) {
       ng_log("Cannot set resource. %s: %s\n", path, strerror(errno));
       return -1;
   }

   getmaxyx(ent->w, y, x);
   ent->resource.x = x - 1; // -1 for box
   ent->resource.y = y - 1;
   if (ng_image_to_ascii(ent->resource.data, x, y, path) == -1) {
       return -1;
   }

   draw_graphics(ent->w, &ent->resource);
   return 0;
}

int ng_del_window(struct ng_entity *ent)
{
    if (!ent) {
        return false;
    }

    if (ent->p) {
        del_panel(ent->p);
        ent->p = NULL;
    }

    if (ent->w) {
        delwin(ent->w);
        ent->w = NULL;
    }

    return true;
}



int ng_add_window(struct ng_entity *ent, int x, int y, int width, int height)
{
    if (!ent) {
        ng_log("Cannot create window for null entity\n");
        return -1;
    }

    if (ent->w) {
        ng_log("Window is already assigned. Delete it firstly.\n");
        return -1;
    }

    ent->w = newwin(height, width, y, x);
    if (!ent->w) {
        ng_log("Cannot create window for entity %s.\n", ent->name);
        return -1;
    }
    box(ent->w, 0, 0);

    ent->p = new_panel(ent->w);
    if (!ent->p) {
        ng_log("Cannot create panel for entity %s.\n", ent->name);
        return -1;
    }

    ent->x = x;
    ent->y = y;
    ent->width = width;
    ent->height = height;

    return 0;
}

void ng_set_entity_visible(struct ng_entity *ent, bool visible)
{
    ent->visible = visible;
}

void ng_delete_entity(struct ng_entity *ent)
{
    int i;

    if (ent->ng_menu.menu) {
        unpost_menu(ent->ng_menu.menu);
        free_menu(ent->ng_menu.menu);
        for (i = 0; i < ent->ng_menu.menu_cnt; i++) {
            if (ent->ng_menu.items[i]) {
                free_item(ent->ng_menu.items[i]);
            }
        }
    }

    if (ent->ng_menu.form) {
        unpost_form(ent->ng_menu.form);
        free_form(ent->ng_menu.form);
        for (i = 0; i < ent->ng_menu.form_cnt; i++) {
            if (ent->ng_menu.fields[i]) {
                free_field(ent->ng_menu.fields[i]);
            }
         }
     }

    ng_del_window(ent);

    ng_unregister_from_input(ent);
    memset(ent, 0, sizeof(struct ng_entity)); // will set T_EMPTY and ng_menu.menu. TODO: check sizeof
}

void ng_delete_all_entities()
{
    int i;

    for (i = 0; i < ENTITIES_MAX; i++) {
        ng_delete_entity(&entity[i]);
    }
}

int ng_max_entities()
{
    return ENTITIES_MAX;
}
    
struct ng_entity *ng_get_entity(int i)
{
    if (i > ENTITIES_MAX) {
        return NULL;
    }
    if (entity[i].type == T_EMPTY) {
        return NULL;
    }

    return &entity[i];
}

struct ng_entity *ng_entity_find(const char *name)
{
    int i;

    if (!name) {
        ng_log("Cannot get entity with NULL name\n");
        return NULL;
    }

    for (i = 0; i < ENTITIES_MAX; i++) {
        if (!strcmp(entity[i].name, name)) {
            return &entity[i];
        }
    }
    
    return NULL;
}

int ng_install_input(input_type_t type, ng_input_cb_t cb)
{
    if (type >= MAX_INPUTS || type < 0) {
        ng_log("Cannot install more inputs\n");
        return -1;
    }

    inputs[type].cb = cb;
    ng_log("Input subsystem installed\n");

    return 0;
}

int ng_register_to_input(struct ng_entity *ent, input_type_t type)
{
    int i;

    if (!ent) {
        ng_log("Cannot register input to NULL entity\n");
        return -1;
    }

    if (type >= MAX_INPUTS || type < 0) {
        ng_log("Cannot register to input type = %d\n", type);
        return -1;
    }

    if (!inputs[type].cb) {
        ng_log("Cannot registered to not-installed input!\n");
        return -1;
    }

    for (i = 0; i < MAX_INPUT_LISTENERS; i++) {
        if (inputs[type].listener[i] == NULL) {
            inputs[type].listener[i] = ent;
            break;
        }
    }

    if (i == MAX_INPUT_LISTENERS) {
        ng_log("Cannot register %s to input %d. Not enough slots.\n", ent->name, type);
        return -1;
    }

    ng_log("Registered entity %s to input %d slot %d\n", ent->name, type, i);

    return 0;
}

int ng_unregister_from_input(struct ng_entity *ent)
{
    int i, j;

    if (!ent) {
        ng_log("Cannot unregister from null entity\n");
        return -1;
    }

    for (i = 0; i < MAX_INPUTS; i++) {
        for (j = 0; j < MAX_INPUT_LISTENERS; j++) {
            if (inputs[i].listener[j] == ent) {
                inputs[i].listener[j] = NULL;
                ng_log("Unregistered entity %s from input\n", ent->name);
            }
        }
    }
}

int ng_get_input(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot get input from null entity\n");
        return -1;
    }

    return ent->input;
}

static void process_input()
{
    int i, n;
    
    for (i = 0; i < MAX_INPUTS; i++) {
        if (inputs[i].cb) {
            inputs[i].cb();
        }
    }

/*    if (is_host) {
        n = read(lsfd, network_buffer, NETWORK_BUF_SIZE);
        if (n == -1) {
            ng_log_once("Error reading from socket %s\n", strerror(errno));
        } else if (n > 0) {
            ng_log("Message received: %s\n", network_buffer);
            ng_send_msg_srv("Welcome!");
        }
    }*/
}

void update_entities()
{
    int i;
    
    for (i = 0; i < ENTITIES_MAX; i++) {
        if (entity[i].type != T_EMPTY  && entity[i].update) {
            entity[i].update(&entity[i], FRAME_RATE_MSEC);
        }
    }
}

void draw()
{
    int i;

    for (i = 0; i < ENTITIES_MAX; i++) {
        if (entity[i].type == T_EMPTY) {
            continue;
        }
        if (entity[i].visible == true) {
            move_panel(entity[i].p, entity[i].y, entity[i].x);
        }
    }
    ng_refresh();
}

void ng_run()
{
    struct timespec start, end, delta;

    while(running) {
        clock_gettime(CLOCK_MONOTONIC, &start);
        process_input();
        update_entities();
        draw();
        clock_gettime(CLOCK_MONOTONIC, &end);
        timespec_diff(&start, &end, &delta);
        if (is_time_elapsed(&delta, FRAME_RATE_MSEC)) {
            usleep((FRAME_RATE_MSEC * 1000) - (delta.tv_nsec / 1000));
        }
        
    }
}

int ng_print_centered(struct ng_entity *ent, int y, const char *text)
{
    int length, x;

    if (!ent) {
        ng_log("Cannot print to null entity\n");
        return -1;
    }

    if (!text) {
        ng_log("Cannot set null text to entity %s\n", ent->name);
        return -1;
    }

    if (y < 0) {
        ng_log("Cannot print to negative y-coordinate for entity %s\n", ent->name);
        return -1;
    }

    if (!ent->w) {
        ng_log("Entity %s doesn't have a window\n", ent->name);
        return -1;
    }

    length = strlen(text);
    x = (ent->width - length) / 2;
    if (mvwprintw(ent->w, y, x, "%s", text) != ERR) {
        return 0;
    }

    ng_log("mvprintw failed for entity %s\n", ent->name);
    return -1;
}

int ng_create_popup_menu(struct ng_entity *ent, struct ng_menu_choices *choices, int menu_cnt, const char *text)
{
    WINDOW *der;
    int i;
    int ret;
    int len = 0;

    if (!ent || !ent->w || !choices || menu_cnt <= 0 || menu_cnt > 3 || !text) {
        ng_log("Cannot create popup. Invalid arguments\n");
        return -1;

    }

    ent->ng_menu.menu_cnt = menu_cnt;
    ent->ng_menu.items = (ITEM **)calloc(menu_cnt + 1, sizeof (ITEM *));
    for (i = 0; i < menu_cnt; i++) {
        ent->ng_menu.items[i] = new_item(choices[i].name, NULL);
        set_item_userptr(ent->ng_menu.items[i], choices[i].cb);
    }
    ent->ng_menu.items[i] = (ITEM *)NULL;

    ent->ng_menu.menu = new_menu((ITEM **)ent->ng_menu.items);
    if (!ent->ng_menu.menu) {
        ng_log("Failed to create menu\n");
        return -1;
    }

    ng_print_centered(ent, 1, text);

    ret = set_menu_win(ent->ng_menu.menu, ent->w);
    if (ret != E_OK) {
        ng_log("set_menu_win failed. ret = %d\n", ret);
        return -1;
    }
    for (i = 0; i < menu_cnt; i++) {
        len += strlen(choices[i].name);
    }
    len += menu_cnt * 3;

    der = derwin(ent->w, 2, len, 3, ent->width / 2 - len / 2);
//  box(der, 0, 0);
    ret = set_menu_sub(ent->ng_menu.menu, der);
    if (ret != E_OK) {
        ng_log("set_menu_sub failed. ret = %d\n", ret);
        return -1;
    }
    ret = set_menu_format(ent->ng_menu.menu, 1, menu_cnt);
    if (ret != E_OK) {
        ng_log("set_menu_format. ret = %d\n", ret);
        return -1;
    }
    ret = set_menu_mark(ent->ng_menu.menu, " ");
    if (ret != E_OK) {
        ng_log("set_menu_mark. ret = %d\n", ret);
        return -1;
    }
    ret = menu_opts_off(ent->ng_menu.menu, O_SHOWDESC);
    if (ret != E_OK) {
        ng_log("menu_opts_off. ret = %d\n", ret);
        return -1;
    }
    ret = post_menu(ent->ng_menu.menu);
    if (ret != E_OK) {
        ng_log("post_menu. ret = %d\n", ret);
        return -1;
    }

    return 0;
}

int ng_create_menu(struct ng_entity *ent, struct ng_menu_choices *choices, int menu_cnt)
{
    int i;
    int ret;

    if (!ent || !ent->w || !choices || menu_cnt <= 0) {
        ng_log("Cannot create menu. Invalid arguments\n");
        return -1;
    }

    ent->ng_menu.menu_cnt = menu_cnt;
    ent->ng_menu.items = (ITEM **)calloc(menu_cnt + 1, sizeof (ITEM *));
    for (i = 0; i < menu_cnt; i++) {
        ent->ng_menu.items[i] = new_item(choices[i].name, NULL);
        set_item_userptr(ent->ng_menu.items[i], choices[i].cb);
    }
    ent->ng_menu.items[i] = (ITEM *)NULL;

    ent->ng_menu.menu = new_menu((ITEM **)ent->ng_menu.items);
    if (!ent->ng_menu.menu) {
        ng_log("Failed to create menu\n");
        return -1;
    }

    ng_print_centered(ent, 0, "MENU");

    ret = set_menu_win(ent->ng_menu.menu, ent->w);
    if (ret != E_OK) {
        ng_log("set_menu_win failed. ret = %d\n", ret);
        return -1;
    }
    ret = set_menu_sub(ent->ng_menu.menu, derwin(ent->w, ent->height - 2 , ent->width - 2, 2, 2)); //TODO: center
    if (ret != E_OK) {
        ng_log("set_menu_sub failed. ret = %d\n", ret);
        return -1;
    }
    ret = set_menu_mark(ent->ng_menu.menu, " * ");
    if (ret != E_OK) {
        ng_log("set_menu_mark failed. ret = %d\n", ret);
        return -1;
    }
    ret = menu_opts_off(ent->ng_menu.menu, O_SHOWDESC);
    if (ret != E_OK) {
        ng_log("menu_optss_off failed. ret = %d\n", ret);
        return -1;
    }
    ret = post_menu(ent->ng_menu.menu);
    if (ret != E_OK) {
        ng_log("post_menu failed. ret = %d\n", ret);
        return -1;
    }

    return 0;
}

int ng_create_form(struct ng_entity *ent, struct ng_form_data *form_data, int form_cnt, struct ng_form_ops *ops)
{
    int i;
    int len = 0, maxlen = 0;

    ent->ng_menu.form_cnt = form_cnt;
    ent->ng_menu.ops = ops;
    ent->ng_menu.user_data = form_data;

    for (i = 0; i < form_cnt; i++) {
        len = strlen(form_data->text);
        if (len > maxlen) {
            maxlen = len;
        }
        ent->ng_menu.fields[i] = new_field(1, form_data[i].field_width, i, maxlen + 4, 0, 0);
        if (i == 0) {
            set_field_back(ent->ng_menu.fields[i], A_STANDOUT);
        }
        field_opts_off(ent->ng_menu.fields[i], O_AUTOSKIP);
    }

    ent->ng_menu.form = new_form(ent->ng_menu.fields);

    set_form_win(ent->ng_menu.form, ent->w);
    set_form_sub(ent->ng_menu.form, derwin(ent->w, 4, ent->width - 2, 1, 1));

    post_form(ent->ng_menu.form);

    for (i = 0; i < form_cnt; i++) {
        mvwprintw(ent->w, i + 1, 2, form_data[i].text);
    }

    ng_print_centered(ent, ent->height - 2, "Apply: right / Cancel: left");

    return 0;
}

static void highlight_form(struct ng_entity *ent)
{
    int i;

    for (i = 0; i < ent->ng_menu.form_cnt; i++) {
        if (ent->ng_menu.fields[i] == current_field(ent->ng_menu.form)) {
             set_field_back(ent->ng_menu.fields[i], A_NORMAL);
        } else {
            set_field_back(ent->ng_menu.fields[i], A_STANDOUT);
        }
    }
}

int ng_update_form(struct ng_entity *ent)
{
    int i;

    switch (ent->input) {
    case KEY_DOWN:
           highlight_form(ent);
           form_driver(ent->ng_menu.form, REQ_NEXT_FIELD);
           form_driver(ent->ng_menu.form, REQ_END_LINE);
           break;
    case KEY_UP:
           highlight_form(ent);
           form_driver(ent->ng_menu.form, REQ_PREV_FIELD);
           form_driver(ent->ng_menu.form, REQ_END_LINE);
           break;
    case KEY_RIGHT:
           form_driver(ent->ng_menu.form, REQ_VALIDATION);
           if (ent->ng_menu.ops->fetch) {
               for (i = 0; i < ent->ng_menu.form_cnt; i++) {
                   strncpy(ent->ng_menu.user_data[i].buf,
                       field_buffer(ent->ng_menu.fields[i], 0), 256); //TODO max buffer size
               }
               ent->ng_menu.ops->fetch(ent);
           }
           break;
    case KEY_LEFT:
           if (ent->ng_menu.ops->quit) {
               ent->ng_menu.ops->quit(ent);
           }
           break;
    case 0:
        break;
    default:
           form_driver(ent->ng_menu.form, ent->input);
           break;
    }

    if (ent) {
        ent->input = 0;
    }

    return 0;
}
int ng_update_menu(struct ng_entity *ent)
{
    int c;
    int ret = E_OK;
    ITEM *cur;
    void (*cb)(struct ng_entity *ent);

    if (!ent) {
        ng_log("Cannot update menu for null entity\n");
        return -1;
    }

    if (!ent->ng_menu.menu) {
        ng_log("Cannot update menu for entity = %s\n", entity->name);
        return -1;
    }

    switch (ent->input) {
    case KEY_UP:
        ret = menu_driver(ent->ng_menu.menu, REQ_UP_ITEM);
        break;
    case KEY_DOWN:
        ret = menu_driver(ent->ng_menu.menu, REQ_DOWN_ITEM);
        break;
    case KEY_LEFT:
        ret = menu_driver(ent->ng_menu.menu, REQ_LEFT_ITEM);
        break;
    case KEY_RIGHT:
        ret = menu_driver(ent->ng_menu.menu, REQ_RIGHT_ITEM);
        break;
    case 10:
        cur = current_item(ent->ng_menu.menu);
        cb = item_userptr(cur);
        cb(ent); // It can invalidate ent
        if (ent != T_EMPTY && ent->ng_menu.menu) {
            ret = pos_menu_cursor(ent->ng_menu.menu);
        }
        break;
     default:
        break;
    }
    
    if (ret != E_OK && ret != E_REQUEST_DENIED) {
        ng_log("Update menu failed. input = %d ret = %d\n", ent->input, ret);
        return -1;
    }

    ent->input = 0;

    return 0;
}

int ng_set_timer(struct ng_entity *ent, int miliseconds)
{
    if (!ent) {
        ng_log("Cannot set timer for null entity\n");
        return -1;
    }

    if (miliseconds < 0) {
        ng_log("Cannot set negative timeout for entity %s\n", entity->name);
        return -1;
    }

    ent->timer.delta = 0;
    ent->timer.miliseconds = (miliseconds < FRAME_RATE_MSEC) ? FRAME_RATE_MSEC : miliseconds;

    return 0;
}

static void timespec_diff(struct timespec *start, struct timespec *end, struct timespec *result)
{
    if (end->tv_nsec >= start->tv_nsec) {
        result->tv_sec = end->tv_sec - start->tv_sec;
        result->tv_nsec = end->tv_nsec - start->tv_nsec;
    } else {
        result->tv_sec = end->tv_sec - start->tv_sec - 1;
        result->tv_nsec = NSEC_PER_SECOND - (start->tv_nsec - end->tv_nsec);
    }
}

static bool is_time_elapsed(struct timespec *ts, int miliseconds)
{
   if (miliseconds * 1000000 > ts->tv_nsec) {
        return true;
   }

   return false;
}

bool ng_timer_expired(struct ng_entity *ent, long int delta)
{
    if (!ent) {
        ng_log("Cannot check timer for null entity\n");
        return false;
    }

    ent->timer.delta += delta;
    if (ent->timer.delta >= ent->timer.miliseconds) {
        return true;
    }

    return false;
}

static bool is_collision(struct ng_entity *a, struct ng_entity *b)
{
    if (a == b) {
        return false;
    }

    if (a->type != T_ACTOR) {
        return false;
    }

    if (b->type != T_BORDER && b->type != T_ACTOR) {
        return false;
    }

    if (b->type == T_BORDER) {
        if (a->x < b->x || a->x + a->width > b->x + b->width ||
            a->y < b->y || a->y + a->height > b->y + b->height) {
            return true;
        } else {
            return false;
        }
    }

    if (a->x < b->x + b->width &&
        a->x + a->width > b->x &&
        a->y < b->y + b->height &&
        a->height + a->y > b->y) {
        return true;
    }

    return false;
}

struct ng_entity *ng_check_collision(struct ng_entity *ent)
{
    int i;

    if (!ent) {
        ng_log("Cannot check collision for null entity\n");
        return NULL;
    }

    for (i = 0; i < ENTITIES_MAX; i++) {
       if (is_collision(ent, &entity[i])) {
           return &entity[i];
       }
    }

    return NULL;
}

bool ng_is_entity_by_name(struct ng_entity *ent, const char *name)
{
    if (!ent || !name) {
        ng_log("Cannot compare nulls\n");
        return false;
    }

    return !(strncmp(ent->name, name, NG_MAX_NAME_LENGTH));
}

int ng_set_visible(struct ng_entity *ent, bool visible)
{
    if (!ent) {
        ng_log("Cannot set visibility for null entity\n");
        return -1;
    }

    ent->visible = visible;

    return 0;
}

int ng_set_speed_x(struct ng_entity *ent, int speed)
{
    if (!ent) {
        ng_log("Cannot set x speed to null entity\n");
        return -1;
    }

    if (speed < 1) {
        ng_log("Cannot set x speed less than 1 for %s\n", entity->name);
        return -1;
    }

    ent->speed_x = speed;

    return 0;
}

int ng_set_speed_y(struct ng_entity *ent, int speed)
{
    if (!ent) {
        ng_log("Cannot set y speed to null entity\n");
        return -1;
    }
    if (speed < 1) {
        ng_log("Cannot set y speed less than 1 for %s\n", entity->name);
        return -1;
    }

    ent->speed_y = speed;

    return 0;
}

int ng_set_speed(struct ng_entity *ent, int speed_x, int speed_y)
{
    int old_speed_x, old_speed_y;

    if (!ent) {
        ng_log("Cannot set speed for null entity\n");
        return -1;
    }

    old_speed_x = ng_get_speed_x(ent);
    old_speed_y = ng_get_speed_y(ent);

    if (ng_set_speed_x(ent, speed_x) == -1 || ng_set_speed_y(ent, speed_y) == -1) {
        ent->speed_x = old_speed_x;
        ent->speed_y = old_speed_y;
        return -1;
    }

    return 0;
}

int ng_get_x(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot get x from null entity\n");
        return -1;
    }

    return ent->x;
}

int ng_get_y(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot get y from null entity\n");
        return -1;
    }

    return ent->y;
}

int ng_get_width(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot get width from null entity\n");
        return -1;
    }

    return ent->width;
}

int ng_get_height(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Canot get height from null entity\n");
        return -1;
    }

    return ent->height;
}

int ng_get_speed_x(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot get y-speed to null entity\n");
        return -1;
    }

    return ent->speed_x;
}

int ng_get_speed_y(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot get y-speed to null entity\n");
        return -1;
    }

    return ent->speed_y;
}

int ng_set_x(struct ng_entity *ent, int x)
{
    if (!ent) {
        ng_log("Cannot set x coordinate for null entity\n");
        return -1;
    }

    if (x < 0) {
        return -1;
    }

    ent->x = x;

    return 0;
}

int ng_set_y(struct ng_entity *ent, int y)
{
    if (!ent) {
        ng_log("Cannot set y coordinate for null entity\n");
        return -1;
    }

    if (y < 0) {
        return -1;
    }

    ent->y = y;

    return 0;
}

int ng_set_position(struct ng_entity *ent, int x, int y)
{
    int old_x, old_y;

    if (!ent) {
        ng_log("Cannot set position to null entity\n");
        return -1;
    }

    old_x = ng_get_x(ent);
    old_y = ng_get_y(ent);

    if (ng_set_x(ent, x) == -1 || ng_set_y(ent, y) == -1) {
        ent->x = old_x;
        ent->y = old_y;
        return -1;
    }

    return 0;
}

ng_dir_t ng_get_dir(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot get direction from null entity\n");
        return NG_UNKNOWN_DIR;
    }

    return ent->dir;
}

int ng_set_dir(struct ng_entity *ent, ng_dir_t dir)
{
    if (!ent) {
        ng_log("Cannot set direction for null entity\n");
        return -1;
    }

    if (dir < 0 || dir > NG_DOWN_RIGHT) {
        ng_log("Cannot set dir = %d to entity %s\n", dir, ent->name);
        return -1;
    }

    ent->dir = dir;

    return 0;
}

int ng_move(struct ng_entity *ent)
{
    if (!ent) {
        ng_log("Cannot move null entity\n");
        return -1;
    }

    if (ent->speed_x <= 0 || ent->speed_y <= 0) {
        ng_log("Zero or negative speeds are not supported. ent = %s\n", ent->name);
        return -1;
    }

    switch (ent->dir) {
    case NG_RIGHT:
        ent->x += ent->speed_x;
        break;
    case NG_LEFT:
        ent->x -= ent->speed_x;
        break;
    case NG_UP:
        ent->y -= ent->speed_y;
        break;
    case NG_DOWN:
        ent->y += ent->speed_y;
        break;
    case NG_DOWN_RIGHT:
        ent->x += ent->speed_x;
        ent->y += ent->speed_y;
        break;
    case NG_DOWN_LEFT:
        ent->x -= ent->speed_x;
        ent->y += ent->speed_y;
        break;
    case NG_UP_RIGHT:
        ent->x += ent->speed_x;
        ent->y -= ent->speed_y;
        break;
    case NG_UP_LEFT:
        ent->x -= ent->speed_x;
        ent->y -= ent->speed_y;
        break;
    default:
        ng_log("Cannot move ent %s in unknown direction\n", ent->name);
        return -1;
        break;
    }

    if (ent->x < 0) {
        ent->x = 0;
        return -1;
    }

    if (ent->y < 0) {
        ent->y = 0;
        return -1;
    }

    return 0;
}

void ng_finish()
{
    running = false;
    ng_delete_all_entities();
    endwin();
}

void ng_beep()
{
    beep();
}

static int ng_wait_for_client(int timeout)
{
    int ret;
    fd_set rfds;

    FD_ZERO(&rfds);
    FD_SET(csfd, &rfds);

    if (timeout > 0) {
        network_timeout.tv_sec = timeout;
    }

    ret = select(csfd + 1, &rfds, NULL, NULL, &network_timeout);
    if (ret == -1) {
        ng_log("select failed: %s\n", strerror(errno));
        close(csfd);
        return -1;
    }
    if (ret == 0) {
        ng_log("Select timeout\n");
        close(csfd);
        return 1;
    }

    lsfd = accept(csfd, (struct sockaddr *)&cli_addr, &clilen);
    if (lsfd == -1) {
        ng_log("ERROR on accept: %s\n", strerror(errno));
        close(csfd);
        return -1;
    }

    fcntl(lsfd, F_SETFL, O_NONBLOCK);
    is_host = true;

    return 0;
}

int ng_host(int port, int timeout)
{
    csfd = socket(AF_INET, SOCK_STREAM, 0);
    if (csfd == -1) {
       ng_log("ERROR opening socket %s\n", strerror(errno));
       return -1;
    }

    bzero((char *)&serv_addr, sizeof(serv_addr));
    portno = port;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    if (bind(csfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1) {
        ng_log("ERROR on binding %s\n", strerror(errno));
        close(csfd);
        return -1;
    }

    fcntl(csfd, F_SETFL, O_NONBLOCK);
    if (listen(csfd, 16) == -1) {
        ng_log("listen failed: %s\n", strerror(errno));
        close(csfd);
        return -1;
    }
    clilen = sizeof(cli_addr);

    bzero(network_buffer, NETWORK_BUF_SIZE);
    network_timeout.tv_sec = DEFAULT_NETWORK_TIMEOUT;
    network_timeout.tv_usec = 0;

    return ng_wait_for_client(timeout);
}

int ng_connect(const char *ip, int port)
{
    csfd = socket(AF_INET, SOCK_STREAM, 0);
    if (csfd == -1) {
        ng_log("socket failed in ng_connect: %s\n", strerror(errno));
        return -1;
    }

    fcntl(csfd, F_SETFL, O_NONBLOCK);
    server = gethostbyname(ip);
    if (!server) {
        ng_log("Cannot get host %s\n", ip);
        close(csfd);
        return -1;
    }

    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr,
        server->h_length);
    serv_addr.sin_port = htons(port);
    if (connect(csfd,(struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1) {
        ng_log("Cannot connect to %s: %s\n", ip, strerror(errno));
        if (errno != EINPROGRESS) {
            close(csfd);
            return -1;
        }
    }

    ng_log("Connected!\n");
    is_client = true;
    return 0;
}

int ng_send_msg_srv(const char *text)
{
    if (write(lsfd, text, strlen(text)) == -1) {
        ng_log("Error while sending message: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

int ng_send_keycode_from_host(const int keycode)
{
    if (write(lsfd, &keycode, sizeof(int)) == -1) {
        ng_log("Error while sending message: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

int ng_send_msg(const char *text)
{
    if (write(csfd, text, strlen(text)) == -1) {
        ng_log("Error while sending message: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

int ng_send_keycode(const int keycode)
{
    if (write(csfd, &keycode, sizeof(int)) == -1) {
        ng_log("Error while sending keycode: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

int ng_rcv_keycode(int *keycode)
{
    int n = read(csfd, keycode, sizeof(int));

    if (n == -1) {
        ng_log_once("ng_rcv_keycode: error reading from socket %s\n", strerror(errno));
        return -1;
    } else if (n > 0) {
        return 0;
    }

    return -1;

}

int ng_rcv_keycode_from_client(int *keycode)
{
    int n = read(lsfd, keycode, sizeof(int));

    if (n == -1) {
        ng_log_once("ng_rcv_keycode_from_client: error reading from socket %s\n", strerror(errno));
        return -1;
    } else if (n > 0) {
        return 0;
    }

    return -1;

}

int ng_rcv_msg(char *buf, int bufsz)
{
    int n = read(csfd, buf, bufsz);

    if (n == -1) {
        ng_log_once("ng_rcv_msg: error reading from socket %s\n", strerror(errno));
        return -1;
    } else if (n > 0) {
        return 0;
    }

    return -1;

}
