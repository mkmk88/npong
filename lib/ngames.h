/**
 * @file ngames.h
 * @brief File containing API for ngames framework.
 *
 * \mainpage ngames framework documentation
 *
 * Ngames is a framework which purpose is to create small terminal games based on the ncurses library.
 * The idea is to hide all raw ncurses calls from user and provide easy-to-use wrappers needed during game development.
 * Instead of operating on ncurses windows or panels the abstraction layer called "entity" is introduced.
 *
 * There are several functional blocks inside the library: 
 * - logger: interface for writing debug messages into text file.
 * - recource loader: a converter of graphic files (.jpg, .png) to ASCII art.
 * - timer: small interface for time management.
 * - entity manager: set of structures and functions for managing basic ngames abstraction layer.
 * - input: interface for using input devices.
 * - menu: API for creating simple menus.
 */
#ifndef _NGAMES_H_
#define _NGAMES_H_
#include <ncurses.h>
#include <panel.h>
#include <menu.h>
#include <form.h>

/** Maximum size of the ASCII art file in bytes. */
#define NG_RESOURCE_SIZE (1024 * 10)

/** Maximum lenght of the unique name of the entity. */
#define NG_MAX_NAME_LENGTH 256

/** @brief Structure that holds processed ASCII image.
  *
  * In ngames framework, "resource" is an ASCII art. There should not be
  * need to use this structure explictly.
  */
struct ng_resource {
    char data[NG_RESOURCE_SIZE]; /**< Buffor for the ASCII art. */
    int x;                       /**< Width of the image. */
    int y;                       /**< Height of the image */
};

/** @brief General type of entity.
  *
  * Entites need to be associated with one of the general type.
  */
typedef enum {
    T_EMPTY = 0,    /**< Default value when entity is not in use. */
    T_ABSTRACT,     /**< Entity that doesn't have graphical representation. */
    T_WINDOW,       /**< Entity that works as window (like pop=up). */
    T_BORDER,       /**< Special type for entity that is used as a space limiter for gameboard.
                         This is needed to calculate collisions correctly, since all other objects
                         will be inside this entity. */
    T_ACTOR         /**< Main type of entites that are part of a gameplay. Those entites have visual
                         representation and can collide. */
} ng_entity_type_t;

/** @brief Direction of movement.
  *
  * Standardizded names of different directions.
  */
typedef enum {
    NG_UNKNOWN_DIR,
    NG_LEFT,
    NG_RIGHT,
    NG_UP,
    NG_DOWN,
    NG_UP_LEFT,
    NG_UP_RIGHT,
    NG_DOWN_LEFT,
    NG_DOWN_RIGHT
} ng_dir_t;

/** @brief Callback for input handling.
  *
  * Function of this type need to be provided while installing
  * new input types.
  */
typedef void (*ng_input_cb_t)(void);

struct ng_entity;

/** @brief Structure describing content of the menu.
  *
  * ngames framework uses ncurses "Menus" and "Forms" libraries to create custom menus.
  * This structure holds internal fields used by "Menus" and "Forms" libraries.
  * Menu can be also created as an interactive form. This should not be accessed
  * directly by user of ngames, but only through provided API.
  */
struct ng_menu {
    MENU *menu;                        /**< Internal handler to menu structure. */
    ITEM **items;                      /**< Internal handler to array of menu items. */
    FORM *form;                        /**< Internal handler to form structure. */
    FIELD *fields[3];                  /**< Internal handler to array of form fields. TODO: max_fields*/
    int menu_cnt;                      /**< Number of items in the menu. */
    int form_cnt;                      /**< Number of fields in form. */
    struct ng_form_data *user_data;    /**< Pointer to data provided by user to display form. */
    struct ng_form_ops *ops;           /**< User callbacks used by forms. */
};

/** @brief Structure to hold menu items.
  *
  * This is used while creating a new ngames menu. User needs to create an array
  * of ng_menu_choices and fill it with pairs "name - callback". This array can be
  * then passed to ng_create_menu or ng_create_pupup_menu.
  */
struct ng_menu_choices {
    char name[NG_MAX_NAME_LENGTH];        /**< Name of the menu item */
    void (*cb)(struct ng_entity *ent);    /**< Callback to function that will be executed when
                                               item "name" is chosen */
};

struct ng_form_data {
    char text[256];
    int field_width;
    char *buf;
};

struct ng_form_ops {
    void (*fetch)(struct ng_entity *ent);
    void (*quit)(struct ng_entity *ent);
};

/** Structure with data needed by ngames timer.
  */
struct ng_timer {
    long int miliseconds;    /**< How many miliseconds need to pass to trigger timer event */
    long int delta;          /**< How many miliseconds passed since last update */
};

int ng_create_form(struct ng_entity *ent, struct ng_form_data *form_data, int form_cnt, struct ng_form_ops *ops);
int ng_update_form(struct ng_entity *ent);

/** @brief Callback used while updating logic related to the given entity.
  *
  * Function provided in this callback will be called at each iteration of the
  * main loop. Its purpose is to do calculation of logic (for instance 
  * calculate a new position of the entity or update score of the game).
  * @param self Pointer to the entity which needs to be updated.
  * @param delta Time in miliseconds since last update.
  */
typedef void (*ng_update_cb_t)(struct ng_entity *self, long int delta);

/** @brief Main structure used by ngames framework.
  *
  * Entity is an object that can (but must not) be displayed, updated and connected
  * to one of the input sources. It can represent an abstract element of the underlaying system
  * (for example rules of the game) or something that is part of visible interface (game board,
  * pop-up window, player, opponent etc.). By design it can represent namely everything in the ngames
  * environment. You should NEVER modify instances of this structure directly. Use setters available
  * in the ngames API.
  */
struct ng_entity {
    WINDOW *w;                    /**< ncurses window used by this entity. */
    PANEL *p;                     /**< ncurses panel used by this entity. */
    ng_entity_type_t type;        /**< Generic type (kind) of the entity. @see ng_entity_type_t. */
    char name[256];               /**< Human-readable name of the entity. Must be unique. */
    bool visible;                 /**< Determines if the window/panel will be actually drawn on the screen. */
    int x;                        /**< Position of the left-top corner of window/panel in x-axis. */
    int y;                        /**< Position of the left-top corner of window/panel in y-axis. */
    int width;                    /**< Width of the window/panel used by this entity. */
    int height;                   /**< Height of the window/panel used by this entity. */
    int speed_x;                  /**< It's a multplier used while moving window/panel in x-axis. */
    int speed_y;                  /**< It's a multplier used while moving window/panel in y-axis.  */
    int dir;                      /**< Default direction of move coded as an integer. */
    int input;                    /**< A data delivered by input subsystem. */
    struct ng_timer timer;        /**< A timer that can be used for general purposes. */
    struct ng_resource resource;  /**< ASCII resource used by this entity if available. */
    ng_update_cb_t update;        /**< @see ng_update_cb_t. */
    struct ng_menu ng_menu;       /**< Data needed if entity is a menu. */
};

/** @brief Possible type of input source.
  *
  * Ngames framework provides two pre-defined types of input. There's no option to add more types of input at this moment.
  * Each type of input needs to retrieve value from its specific source and store it into ng_entity.input. Entites can be then
  * registered to the given input type to listen for those input events.
  */
typedef enum {
    I_KEYBOARD,    /**< Input from keyboard, enabled by default. */
    I_NETWORK,     /**< Input from network. TODO. */
} input_type_t;

/**
  * @brief Initialize ngames framework.
  *
  * This function must be called as a first one. It initalizes ncurses and all internal stuff.
  * @return 0 on success -1 otherwise.
  */
int ng_init(void);

/** Get terminal width.
  *
  * @return Number of columns of terminal.
  */
int ng_get_terminal_width(void);

/** Get terminal height.
  *
  * @return Number of rows of terminal.
  */
int ng_get_terminal_height(void);

/** @brief Update framework
  *
  * This function need to be called each time you want to actually draw something into screen.
  * Use this function instead of ncurses refresh().
  */
void ng_refresh(void);

/**
 * Put an entry into ngame logger. Use standard formatting as input.
 */
void ng_log(const char *fmt, ...);

/**
 * Limited variant of nlog. Put only one message into the log, no matter how often it's called.
 * @see nlog.
 */
void ng_log_once(const char *fmt, ...);

/**
 * Convert an image from file located at @p path into ASCII art and store it into the @p buf.
 * @param buf Output buffer of size large enough to store @p x * @p y. It will be filled with ASCII art content.
 * @param x Width of the output ASCII art
 * @param y Height of the output ASCII art
 * @param path Path to the graphic file (.jpg or .png);
 * @return 0 on success -1 otherwise
 */
int ng_image_to_ascii(char *buf, int x, int y, const char *path);

/** @brief Add a default entity
  *
  * Default entity has only type and name specified. It will not have ncurses window associated, so no
  * graphical representation. It will also not be updated unless you explicitly provide update callback (using:todo).
  * Usually you want to use this function and set needed features (like windows or input connections) later in the time.
  * It's also good starting point for creating any abstract (not displayed) entities.
  * @param type Refer to ng_entity_type_t.
  * @param name Refer to ng_entity.name.
  * @return Pointer to the valid entity on success or NULL otherwise.
  */
struct ng_entity *ng_add_entity(ng_entity_type_t type, const char *name);

/** @brief Set function that will be called at each update.
  *
  * This is important. The logic of the game should be implemented in update callbacks
  * of each entity. Those callbacks will be called at each game loop iteration. This
  * function sets the callback to the entity.
  * @param ent Entity to which you want connect the update callback.
  * @param update_cb Function that implements update logic of entity.
  * @return 0 on success -1 otherwise.
  */
int ng_set_update_cb(struct ng_entity *ent, ng_update_cb_t update_cb);

/** 
 * @brief Load texture to entity.
 *
 * Load a graphic file located at @p path and store it as ASCII art in the entity @p ent.
 * @param ent Pointer to the entity to which you want add a graphic resource.
 * @param path Path to the grahpic resource you want to load.
 * @return 0 on success -1 otherwise.
 */
int ng_load_image(struct ng_entity *ent, const char *path);

/**
  * @brief Fill window with given character.
  *
  * If entity has a window, fill it entirely with a single character.
  * @parem ent Pointer to the entity which needs to be filled.
  * @param c Character with which entity's window need to be filled.
  * @return 0 on success -1 otherwise.
  */
int ng_fill_window(struct ng_entity *ent, const char c);

/**
  * @brief Delete window.
  *
  * If entity @p ent has a window, this function will remove (unset) this window.
  * @param ent Entity from which you want delete window.
  * @return 0 on success -1 otherwise.
  */
int ng_del_window(struct ng_entity *ent);

/**
  * @brief Add ncurses window to the entity
  *
  * Create an ncurses window of given dimensions and assign it to the entity @p ent.
  * Note: technically it will create also ncurses panel but it shouldn't bother you.
  * If the window is already created, you must firstly delete it using ng_del_window.
  * @param ent Pointer to the entity to which you want to assign a new window.
  * @param x Refer to ng_entity.x.
  * @param y Refer to ng_entity.y.
  * @param width Refer to ng_entity.width.
  * @param height Refer to ng_entity.height.
  * @return 0 on success -1 otherwise
  */
int ng_add_window(struct ng_entity *ent, int x, int y, int width, int height) ; 

/**
  * Simply setter of ng_entity.visible.
  *
  * @param ent Pointer to the entity of which visiblity you want to change.
  * @param visible New value of @p ent's visibilty.
  * @return Never fails. Just like the whole lib. 
  */
void ng_set_entity_visible(struct ng_entity *ent, bool visible);

/** @brief Remove an unnecessary entity
  *
  * Remove entity from the internal ngames framework structures. Destroy associated ncurses window
  * and unregister from input sources. Free all other resources kept by @p ent. Entity @p ent will be no longer
  * accessible or valid after call to this function.
  * @param ent Pointer to the entity which you want to delete.
  * @return Never fails.
  */
void ng_delete_entity(struct ng_entity *ent);

/** @brief Delete all entites.
  *
  * Remove all previously registered (added) entites from ngames framework internal structures.
  * Can be used while resetting state of the game.
  */
void ng_delete_all_entities(void);

/** @brief Get the maximum number of available entities
  *
  * This is the upper limit of entities in the ngames framework that can be used at one time. This limit is
  * arbitrary chosen by framework author (me).
  * TODO: the data structure for entites will be changed to list.
  * @see ng_get_entity.
  *
  * @return Positive integer, number of total possible slots for entities.
  */
int ng_max_entities(void);

/** @brief Get entity by index
  *
  * All entites can be accessible by index number from 0 to ng_max_entites. You can use this function while
  * iterating over all available entites. This function will return only valid entities or NULL pointer.
  * Valid entites are those added correctly with use of one of the existing ngames API functions like 
  * ng_add_entity or ng_add_entity_default.
  * @param i index number from 0 to ng_max_entities. TODO: foreach.
  * @return Valid entity or NULL otherwise.
  */
struct ng_entity *ng_get_entity(int i);

/** @brief Find entity using human-readable label
  *
  * Each entity can be identified by unique string literal. This literal must be provided while adding
  * new entity. This function returns entity matching @p name.
  * @param name Unique string literal of entity you want to retrive.
  * @return Pointer to the entity or NULL value if it's not present.
  */
struct ng_entity *ng_entity_find(const char *name);

/** @brief Add new type of input to the ngames framework.
  *
  * If you want to implement new source of input you need to add it to the ngames framework using this function.
  * By default, there is already input from keyboard installed.
  * @param type One of the pre-defined types of input.
  * @param cb Callback to function which will actually retrieve the input.
  * @return 0 on success -1 otherwise.
  */
int ng_install_input(input_type_t type, ng_input_cb_t cb);

/** @brief Register entity to one of the input sources.
  *
  * If you register entity @p ent into one of the installed input sources, this entity will
  * listen events from it at each iteration of main loop. Those events will be stored in ng_entity.input.
  * @param ent Pointer to entity which will be listening to the given input source.
  * @param type Type of the input source to which you want to register.
  * @return 0 on success -1 otherwise.
  */
int ng_register_to_input(struct ng_entity *ent, input_type_t type);

/** @brief Unregister entity from listening to the input source.
  *
  * Entity @p ent will no longer listen to any input source it is registered to.
  * @param ent Pointer to entity you want to unregister from input sources.
  * @return 0 on success -1 otherwise.
  */
int ng_unregister_from_input(struct ng_entity *ent);

/** @brief Get data from input source.
  *
  * Get data (usually a character) of input to which entity @p ent is registered.
  * @param ent Entity which received the input event.
  * @return Input character or -1 on error.
  */
int ng_get_input(struct ng_entity *ent);

/** @brief Run main loop.
 *
 * This function will start the main loop of the game. Starting main loop means processing input,
 * udpatig logic of each entity and drawing them into the screen.
 */
void ng_run(void);

/** @brief Create menu window.
  *
  * Two steps are needed to created new ngmaes menu. Firstly, user needs to create a new T_WINDOW
  * entity with size suitable to hold menu items. Secondly, user needs to create *static* array of
  * ng_menu_choices and fill them with appropriate names and callbacks. After thath, ng_create_menu
  * can be called. This function must be called before updating the menu. @see ng_update_menu.
  *
  * @param ent T_WINDOW entity which will be used as menu.
  * @param choices array of menu itmes with action callbacks.
  * @param menu_cnt Size of @p choices array - number of menu items.
  * @return 0 on success -1 otherwise.
  */
int ng_create_menu(struct ng_entity *ent, struct ng_menu_choices *choices, int menu_cnt);

/** @brief Create small popup menu.
  *
  * This function makes entity @p ent a popup window. Popup window is a small window with short text message
  * and up to two choices layed out horizontaly. Usually those choices will be "yes" and "no". Use of this
  * function is similar to ng_create_menu. @see ng_create_menu @see ng_update_menu.
  * @param ent T_WINDOW entity which will be used as popup menu.
  * @param choices array of menu items with action callbacks (maximum 3).
  * @param menu_cnt Size of @p choices array - number of menu items.
  * @param text Short text message that will be displayed in the middle of popup window.
  * @return 0 on success -1 otherwise.
  */
int ng_create_popup_menu(struct ng_entity *ent, struct ng_menu_choices *choices, int menu_cnt, const char *text);

/** @brief Update menu logic.
  *
  * After creating menu with use of ng_create_menu or ng_create_popup_menu there is last additional
  * step to make ngames menu fully functional. User needs to run ng_update_menu from ng_update_cb
  * associated with entity @p ent. So, basically this function will be always run from update callback.
  * @param ent T_WINDOW entity with menu created by ng_create_menu or ng_create_popup_menu.
  * @return 0 on sucess -1 otherwise.
  */
int ng_update_menu(struct ng_entity *ent);

/** @brief Print centered text.
  *
  * Print text in the middle of window with the offset @p y in y-axis.
  * @param ent Entity (usually T_WINDOW) that holds the window to which you want to add text.
  * @param y Offset in lines. Value "0" means text will be printed on top of the window.
  * it cannot be negative.
  * @param text Text to be displayed.
  * @return 0 on success -1 otherwise.
  */
int ng_print_centered(struct ng_entity *ent, int y, const char *text);


/** @brief Program timer.
  *
  * Each entity can program simple timer for given timeout. It then can be checked
  * with use of ng_timer_expired.
  * @param ent Entity for which you want to measure time.
  * @param miliseconds Timeout in miliseconds (must be positive value).
  * @return 0 on success -1 otherwise.
  */
int ng_set_timer(struct ng_entity *ent, int miliseconds);

/** @brief Check if timer expired.
  *
  * If entity has a programmed timeout by ng_set_timer, then it can be checked
  * using this function.
  * @param ent Entity with a programmed timer.
  * @param delta Miliseconds since last update. This value is passed by ngames framework
  * as an argument to ng_update_cb.
  * @return True if timer programmed by ng_set_timer expired, false if not.
  */
bool ng_timer_expired(struct ng_entity *ent, long int delta);

/** @brief Check for collision.
  *
  * Check if T_ACTOR entity @p ent has collided with other T_ACTOR entity
  * or with T_BORDER entity. Collision occurs when entity's graphic
  * representation limited with x, y, width and height values overlaps
  * with other entity's graphical representation. This function returns if
  * detects first collision.
  * @param ent Entity to check for collision.
  * @return Pointer to entity with which @p ent collided or NULL pointer if
  * there was no collision.
  */
struct ng_entity *ng_check_collision(struct ng_entity *ent);

/** @brief Check if entity has a given name.
  *
  * Helper to check if entity's name matches string literal passed
  * in argument @p name.
  * @param ent Entity to check the name.
  * @param name String literal to compare.
  * @return True if name of the entity @p ent equals to @p name, false
  * oterhwise.
  */
bool ng_is_entity_by_name(struct ng_entity *ent, const char *name);

/** @brief Set visibilty of entity.
  *
  * Set visible parameter of entity wich has a graphical representation.
  * Entity that has visiblity set to 'false' will not be drawn.
  * @param ent Entity to modify.
  * @param visible "True" to make entity visible "false" otherwise.
  * @return 0 on success -1 otherwise
  */
int ng_set_visible(struct ng_entity *ent, bool visible);

/** @brief Get x position of entity.
  *
  * 'x' is a top-left cooridnates of entity in x-axis.
  * @param ent Entity to read from.
  * @return x cooridnate or -1 on error.
  */
int ng_get_x(struct ng_entity *ent);

/** @brief Get y position of entity.
  *
  * 'y' is a top-left cooridnates of entity in y-axis.
  * @param ent Entity to read from.
  * @return y cooridnate or -1 on error.
  */
int ng_get_y(struct ng_entity *ent);

/** @brief Get width of entity.
  *
  * 'width' is a number of columns that graphical representation of
  * entity @p ent has.
  * @param ent Entity to read from.
  * @return width value or -1 on error.
  */
int ng_get_width(struct ng_entity *ent);

/** @brief Get height of entity.
  *
  * 'height' is a number of rows that graphical representation of
  * entity @p ent has.
  * @param ent Entity to read from.
  * @return height value or -1 on error.
  */
int ng_get_height(struct ng_entity *ent);

/** @brief Set x cooridnate.
  *
  * 'x' coordinate is a top-left corner of graphical representation
  * of entity in x-axis.
  * @param ent Entity to modify.
  * @param x Coordinate in x-axis (must be positive value).
  * @return 0 on success -1 otherwise.
  */
int ng_set_x(struct ng_entity *ent, int x);

/** @brief Set y cooridnate.
  *
  * 'y' coordinate is a top-left corner of graphical representation
  * of entity in y-axis.
  * @param ent Entity to modify.
  * @param y Coordinate in y-axis (must be positive value).
  * @return 0 on success -1 otherwise.
  */
int ng_set_y(struct ng_entity *ent, int y);

/** @brief Set position of entity.
  *
  * Set new coordinates of entity.
  * @param ent Entity to modify.
  * @param x New coordinate in x-axis (must be positive value).
  * @param y New coordinate in y-axis (must be postiive value).
  * @return 0 on success -1 otherwise.
  */
int ng_set_position(struct ng_entity *ent, int x, int y);

/** Set movement speed in x-axis.
  *
  * @param ent Entity to read from.
  * @param speed Speed in x-axis (must be positive value).
  * @return 0 on success -1 otherwise.
  */
int ng_set_speed_x(struct ng_entity *ent, int speed);

/** Set movement speed in y-axis.
  *
  * @param ent Entity to modify.
  * @param speed Speed in y-axis (must be positive value).
  * @return 0 on success -1 otherwise.
  */
int ng_set_speed_y(struct ng_entity *ent, int speed);

/** Set movement speed for entity.
  *
  * @param ent Entity to modify.
  * @param speed_x Speed in x-axis (must be positive value).
  * @param speed_y Speed in y-axis (must be positive value).
  * @return 0 on success -1 otherwise.
  */
int ng_set_speed(struct ng_entity *ent, int speed_x, int speed_y);

/** Get speed in x-axis.
  *
  * @param ent Entity to read from.
  * @return Speed in x-axis or -1 on error.
  */
int ng_get_speed_x(struct ng_entity *ent);

/** Get speed in y-axis.
  *
  * @param ent Entity to read from.
  * @return Speed in y-axis or -1 on error.
  */
int ng_get_speed_y(struct ng_entity *ent);

/** Get direction of movement.
  *
  * @param ent Entity to read from.
  * @return Direction of movement in ngames format. If entity
  * is incorrect, NG_UNKNOWN_DIR will be returned.
  */
ng_dir_t ng_get_dir(struct ng_entity *ent);

/** Set new direction of movement.
  *
  * @param ent Entity to modify.
  * @param dir New direction in ngmames format.
  * @return 0 on success -1 otherwise.
  */
int ng_set_dir(struct ng_entity *ent, ng_dir_t dir);

/** @brief Make a move of entity.
  *
  * This function will update entity's position accordingly to its
  * current direction and speed of the movement.
  * @param ent Entity to move.
  * @return 0 on success -1 otherwise.
  */
int ng_move(struct ng_entity *ent);

/** @brief Finish ngames framework.
  *
  * Stop the main loop (TODO) and close ncurses window.
  */
void ng_finish(void);

/** @brief Play the sound.
  *
  * Play the 'beep' sound from PC Speaker. If terminal doesn't support
  * it, this function does nothing.
  */
void ng_beep(void);

int ng_host(int port, int timeout);
int ng_connect(const char *ip, int port);
int ng_send_msg(const char *text); // temporary
int ng_send_keycode(const int keycode); // temporary
int ng_send_keycode_from_host(const int keycode); // temporary
int ng_rcv_msg(char *buf, int bufsz);
#endif
