#include <stdlib.h>
#include <check.h>
#include "../ngames.h"

void create_test_entity(struct ng_entity *ent)
{
    strcpy(ent->name, "test");
    ent->type = T_EMPTY;
    ent->x = 1;
    ent->y = 1;
    ent->speed_x = 1;
    ent->speed_y = 1;
    ent->dir = NG_UNKNOWN_DIR;
}

START_TEST(test_ng_move)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_move(NULL) == -1);

    // unknown dir
    ck_assert(ng_move(&ent) == -1);

    // wrong speed
    ent.speed_x = -2;
    ck_assert(ng_move(&ent) == -1);
    ent.speed_x = 2;
    ent.speed_y = -1;
    ck_assert(ng_move(&ent) == -1);

    // negative coordinates
    ent.dir = NG_LEFT;
    ent.x = 0;
    ent.speed_x = 1;
    ck_assert(ng_move(&ent) == -1);
    ent.dir = NG_UP;
    ent.y = 0;
    ent.speed_y = 1;
    ck_assert(ng_move(&ent) == -1);

    // correct case;
    ent.dir = NG_LEFT;
    ent.speed_x = 1;
    ent.speed_y = 1;
    ent.x = 1;
    ent.y = 1;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 0 && ent.y == 1);
    ent.dir = NG_RIGHT;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 1 && ent.y == 1);
    ent.dir = NG_DOWN;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 1 && ent.y == 2);
    ent.dir = NG_DOWN_RIGHT;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 2 && ent.y == 3);
    ent.dir = NG_DOWN_LEFT;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 1 && ent.y == 4);
    ent.dir = NG_UP_RIGHT;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 2 && ent.y == 3);
    ent.dir = NG_UP_LEFT;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 1 && ent.y == 2);
    ent.dir = NG_UP;
    ck_assert(ng_move(&ent) == 0);
    ck_assert(ent.x == 1 && ent.y == 1);

}
END_TEST

START_TEST(test_ng_set_dir)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_set_dir(NULL, NG_LEFT) == -1);
    ck_assert(ng_set_dir(&ent, -1) == -1);
    ck_assert(ng_set_dir(&ent, NG_DOWN_RIGHT + 1) == -1);
    ck_assert(ng_set_dir(&ent, NG_LEFT) == 0);
    ck_assert(ent.dir == NG_LEFT);
    ck_assert(ng_set_dir(&ent, NG_UNKNOWN_DIR) == 0);
    ck_assert(ent.dir == NG_UNKNOWN_DIR);
    ck_assert(ng_set_dir(&ent, NG_DOWN_RIGHT) == 0);
    ck_assert(ent.dir == NG_DOWN_RIGHT);

}
END_TEST

START_TEST(test_ng_get_dir)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_dir(NULL) == NG_UNKNOWN_DIR);
    ent.dir = NG_RIGHT;
    ck_assert(ng_get_dir(&ent) == NG_RIGHT);

}
END_TEST

START_TEST(test_ng_set_position)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ent.x = 2;
    ent.y = 2;

    ck_assert(ng_set_position(NULL, 1, 1) == -1);
    ck_assert(ng_set_position(&ent, -1, 1) == -1);
    ck_assert(ent.x == 2 && ent.y == 2);
    ck_assert(ng_set_position(&ent, -1, -1) == -1);
    ck_assert(ent.x == 2 && ent.y == 2);
    ck_assert(ng_set_position(&ent, 0, -1) == -1);
    ck_assert(ent.x == 2 && ent.y == 2);
    ck_assert(ng_set_position(&ent, 1, 1) == 0);
    ck_assert(ent.x == 1 && ent.y == 1);

}
END_TEST

START_TEST(test_ng_set_x)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ent.x = 2;

    ck_assert(ng_set_x(NULL, 1) == -1);
    ck_assert(ent.x == 2);
    ck_assert(ng_set_x(&ent, -1) == -1);
    ck_assert(ent.x == 2);
    ck_assert(ng_set_x(&ent, 0) == 0);
    ck_assert(ent.x == 0);

}
END_TEST

START_TEST(test_ng_set_y)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ent.y = 2;

    ck_assert(ng_set_y(NULL, 1) == -1);
    ck_assert(ent.y == 2);
    ck_assert(ng_set_y(&ent, -1) == -1);
    ck_assert(ent.y = 2);
    ck_assert(ng_set_y(&ent, 0) == 0);
    ck_assert(ent.y == 0);

}
END_TEST

START_TEST(test_ng_get_speed_x)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_speed_x(NULL) == -1);
    ent.speed_x = 4;
    ck_assert(ng_get_speed_x(&ent) == 4);

}
END_TEST

START_TEST(test_ng_get_speed_y)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_speed_y(NULL) == -1);
    ent.speed_y = 4;
    ck_assert(ng_get_speed_y(&ent) == 4);

}
END_TEST

START_TEST(test_ng_set_speed)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ent.speed_x = 2;
    ent.speed_y = 2;

    ck_assert(ng_set_speed(NULL, 1, 1) == -1);
    ck_assert(ng_set_speed(&ent, 0, 1) == -1);
    ck_assert(ent.speed_x == 2 && ent.speed_y == 2);
    ck_assert(ng_set_speed(&ent, 0, 0) == -1);
    ck_assert(ent.speed_x == 2 && ent.speed_y == 2);
    ck_assert(ng_set_speed(&ent, 1, 0) == -1);
    ck_assert(ent.speed_x == 2 && ent.speed_y == 2);
    ck_assert(ng_set_speed(&ent, 1, 1) == 0);
    ck_assert(ent.speed_x == 1 && ent.speed_y == 1);

}
END_TEST

START_TEST(test_ng_set_speed_x)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ent.speed_x = 2;

    ck_assert(ng_set_speed_x(NULL, 1) == -1);
    ck_assert(ent.speed_x == 2);
    ck_assert(ng_set_speed_x(&ent, 0) == -1);
    ck_assert(ent.speed_x == 2);
    ck_assert(ng_set_speed_x(&ent, 1) == 0);
    ck_assert(ent.speed_x == 1);

}
END_TEST

START_TEST(test_ng_set_speed_y)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ent.speed_y = 2;

    ck_assert(ng_set_speed_y(NULL, 1) == -1);
    ck_assert(ent.speed_y == 2);
    ck_assert(ng_set_speed_y(&ent, 0) == -1);
    ck_assert(ent.speed_y == 2);
    ck_assert(ng_set_speed_y(&ent, 1) == 0);
    ck_assert(ent.speed_y == 1);

}
END_TEST

START_TEST(test_ng_get_x)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_x(NULL) == -1);
    ent.x = 4;
    ck_assert(ng_get_x(&ent) == 4);

}
END_TEST

START_TEST(test_ng_get_y)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_y(NULL) == -1);
    ent.y = 4;
    ck_assert(ng_get_y(&ent) == 4);

}
END_TEST

START_TEST(test_ng_get_width)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_width(NULL) == -1);
    ent.width = 4;
    ck_assert(ng_get_width(&ent) == 4);

}
END_TEST

START_TEST(test_ng_get_height)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_height(NULL) == -1);
    ent.height = 4;
    ck_assert(ng_get_height(&ent) == 4);

}
END_TEST

START_TEST(test_ng_set_visible)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ent.visible = false;

    ck_assert(ng_set_visible(NULL, true) == -1);
    ck_assert(ent.visible == false);
    ck_assert(ng_set_visible(&ent, true) == 0);
    ck_assert(ent.visible == true);

}
END_TEST

START_TEST(test_ng_is_entity_by_name)
{
    struct ng_entity ent;
    int i;
    char big_string[NG_MAX_NAME_LENGTH * 2];

    for (i = 0; i < sizeof(big_string); i++) {
        big_string[i] = 'a';
    }

    create_test_entity(&ent);

    ck_assert(ng_is_entity_by_name(NULL, "a") == false);
    ck_assert(ng_is_entity_by_name(&ent, NULL) == false);
    ck_assert(ng_is_entity_by_name(&ent, "test") == true);
    ck_assert(ng_is_entity_by_name(&ent, big_string) == false);

}
END_TEST

START_TEST(test_ng_check_collision)
{
    // Only two possible entites types can collide:
    // T_ACTOR with T_ACTOR
    // T_ACTOR with T_BORDER
    struct ng_entity *test, *obstacle, *border, *window;

    // Need to setup entites array.
    test = ng_add_entity(T_ACTOR, "test");
    obstacle = ng_add_entity(T_ACTOR, "obstacle");
    border = ng_add_entity(T_BORDER, "border");
    window = ng_add_entity(T_WINDOW, "window");

    ck_assert(ng_check_collision(NULL) == NULL);

    border->x = 1;
    border->width = 20;
    border->y = 1;
    border->height = 20;

    window->x = 4;
    window->width = 10;
    window->y = 4;
    window->height = 10;

    test->x = 6;
    test->width = 4;
    test->y = 6;
    test->height = 4;

    // no overlap
    obstacle->x = 12;
    obstacle->width = 4;
    obstacle->y = 6;
    obstacle->height = 4;
    ck_assert(ng_check_collision(test) == NULL);

    // touch from right (not a collision yet)
    obstacle->x = test->x + test->width;
    obstacle->y = test->y;
    ck_assert(ng_check_collision(test) == NULL);

    // touch from bottom (not a collision yet)
    obstacle->x = test->x;
    obstacle->y = test->y + test->height;
    ck_assert(ng_check_collision(test) == NULL);

    // touch from left (not a collision yet)
    obstacle->x = test->x - obstacle->width;
    obstacle->y = test->y;
    ck_assert(ng_check_collision(test) == NULL);

    // touch from top (not a collision yet)
    obstacle->x = test->x;
    obstacle->y = test->y - obstacle->height;
    ck_assert(ng_check_collision(test) == NULL);

    // overlap from right
    obstacle->x = test->x + test->width - 1;
    obstacle->y = test->y;
    ck_assert(ng_check_collision(test) == obstacle);

    // overlap from bottom
    obstacle->x = test->x;
    obstacle->y = test->y + test->height - 1;
    ck_assert(ng_check_collision(test) == obstacle);

    // overlap from left
    obstacle->x = (test->x - obstacle->width) + 1;
    obstacle->y = test->y;
    ck_assert(ng_check_collision(test) == obstacle);

    // overlap from top
    obstacle->x = test->x;
    obstacle->y = (test->y - obstacle->height) + 1;
    ck_assert(ng_check_collision(test) == obstacle);

    // overlap partially from right
    obstacle->x = test->x + test->width - 1;
    obstacle->y = test->y + 2;
    ck_assert(ng_check_collision(test) == obstacle);

    // overlap partially from bottom
    obstacle->x = test->x + 2;
    obstacle->y = test->y + test->height - 1;
    ck_assert(ng_check_collision(test) == obstacle);

    // overlap partially from left
    obstacle->x = (test->x - obstacle->width) + 1;
    obstacle->y = test->y + 2;
    ck_assert(ng_check_collision(test) == obstacle);

    // overlap partially from top
    obstacle->x = test->x + 2;
    obstacle->y = (test->y - obstacle->height) + 1;
    ck_assert(ng_check_collision(test) == obstacle);

    ng_delete_entity(obstacle);

    // touch border from right (not collision yet)
    test->x = (border->x + border->width) - test->width;
    test->y = 2;
    ck_assert(ng_check_collision(test) == NULL);

    // touch border from bottom (not collision yet)
    test->x = 2;
    test->y = border->y;
    ck_assert(ng_check_collision(test) == NULL);

    // touch border from left (not collision yet)
    test->x = border->x;
    test->y = 2;
    ck_assert(ng_check_collision(test) == NULL);

    // touch border from top (not collision yet)
    test->x = 2;
    test->y = (border->y + border->height) - test->height;
    ck_assert(ng_check_collision(test) == NULL);

    // collide with border from right
    test->x = ((border->x + border->width) - test->width) + 1;
    test->y = 2;
    ck_assert(ng_check_collision(test) == border);

    // touch border from bottom (not collision yet)
    test->x = 2;
    test->y = border->y - 1;
    ck_assert(ng_check_collision(test) == border);

    // touch border from left (not collision yet)
    test->x = border->x - 1;
    test->y = 2;
    ck_assert(ng_check_collision(test) == border);

    // touch border from top (not collision yet)
    test->x = 2;
    test->y = ((border->y + border->height) - test->height) + 1;
    ck_assert(ng_check_collision(test) == border);

    // no collision with T_WINDOW
    test->x = 3;
    test->y = 4;
    ck_assert(ng_check_collision(test) == NULL);

}
END_TEST

START_TEST(test_ng_timer_expired)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_timer_expired(NULL, 1) == false);
    ent.timer.delta = 0;
    ent.timer.miliseconds = 500;
    ck_assert(ng_timer_expired(&ent, 100) == false);
    ck_assert(ng_timer_expired(&ent, 100) == false);
    ck_assert(ng_timer_expired(&ent, 100) == false);
    ck_assert(ng_timer_expired(&ent, 100) == false);
    ck_assert(ng_timer_expired(&ent, 100) == true);
    ck_assert(ng_timer_expired(&ent, 100) == true);
    ent.timer.delta = 0;
    ck_assert(ng_timer_expired(&ent, 600) == true);

}
END_TEST

START_TEST(test_ng_set_timer)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_set_timer(NULL, 1) == -1);
    ck_assert(ng_set_timer(&ent, -100) == -1);
    ck_assert(ng_set_timer(&ent, 200) == 0);
    ck_assert(ent.timer.delta == 0);
    ck_assert(ent.timer.miliseconds == 200);

}
END_TEST

START_TEST(test_ng_print_centered)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_print_centered(NULL, 1, "text") == -1);
    ent.w = NULL;
    ck_assert(ng_print_centered(&ent, 1, "text") == -1);
    ent.w = (void *)1;
    ck_assert(ng_print_centered(&ent, -1, "text") == -1);
    ck_assert(ng_print_centered(&ent, 2, NULL) == -1);

}
END_TEST

START_TEST(test_ng_update_menu)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_update_menu(NULL) == -1);
    ent.ng_menu.menu = NULL;
    ck_assert(ng_update_menu(&ent) == -1);

}
END_TEST

START_TEST(test_ng_create_menu)
{
    struct ng_entity ent;
    struct ng_menu_choices choices[2] = {
        {"test1", NULL},
        {"test2", NULL},
    };

    create_test_entity(&ent);

    ck_assert(ng_create_menu(NULL, choices, 1) == -1);
    ck_assert(ng_create_menu(&ent, choices, 1) == -1);
    ent.w = (WINDOW *)1;
    ck_assert(ng_create_menu(&ent, NULL, 1) == -1);
    ck_assert(ng_create_menu(&ent, choices, 0) == -1);

}
END_TEST

START_TEST(test_ng_create_popup_menu)
{
    struct ng_entity ent;
    struct ng_menu_choices choices[2] = {
        {"test1", NULL},
        {"test2", NULL},
    };

    create_test_entity(&ent);

    ck_assert(ng_create_popup_menu(NULL, choices, 1, "text") == -1);
    ck_assert(ng_create_popup_menu(&ent, choices, 1, "text") == -1);
    ent.w = (WINDOW *)1;
    ck_assert(ng_create_popup_menu(&ent, NULL, 1, "text") == -1);
    ck_assert(ng_create_popup_menu(&ent, choices, 0, "text") == -1);
    ck_assert(ng_create_popup_menu(&ent, choices, 4, "text") == -1);
    ck_assert(ng_create_popup_menu(&ent, choices, 1, NULL) == -1);

}
END_TEST

START_TEST(test_ng_get_input)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_get_input(NULL) == -1);

}
END_TEST

START_TEST(test_ng_unregister_from_input)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ck_assert(ng_unregister_from_input(NULL) == -1);

}
END_TEST

START_TEST(test_ng_register_to_input)
{
    struct ng_entity ent;

    create_test_entity(&ent);

    ng_install_input(I_NETWORK, (ng_input_cb_t)1);

    ck_assert(ng_register_to_input(NULL, I_KEYBOARD) == -1);
    ck_assert(ng_register_to_input(&ent, I_NETWORK + 1) == -1);
    ck_assert(ng_register_to_input(&ent, - 1) == -1);
    ck_assert(ng_register_to_input(&ent, I_NETWORK) == 0);

}
END_TEST

START_TEST(test_ng_install_input)
{
    ck_assert(ng_install_input(1024, NULL) == -1);
    ck_assert(ng_install_input(-1, NULL) == -1);
    ck_assert(ng_install_input(I_KEYBOARD, (ng_input_cb_t)1) == 0);

}
END_TEST

START_TEST(test_ng_entity_find)
{
    struct ng_entity *test1 = ng_add_entity(T_ACTOR, "Test1");
    struct ng_entity *test2 = ng_add_entity(T_ACTOR, "Test2");

    ck_assert(ng_entity_find("Test3") == NULL);
    ck_assert(ng_entity_find(NULL) == NULL);
    ck_assert(ng_entity_find("Test2") == test2);
    ck_assert(ng_entity_find("Test1") == test1);

    ng_delete_all_entities();

}
END_TEST

START_TEST(test_ng_add_entity)
{
    ck_assert(ng_add_entity(T_ACTOR, "test") != NULL);
    ck_assert(ng_add_entity(T_ACTOR, "test") == NULL);
    ck_assert(ng_add_entity(T_WINDOW, "test2") != NULL);

    ng_delete_all_entities();

}
END_TEST

Suite *ngames_suite()
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("ngames");

    tc_core = tcase_create("core");

    tcase_add_test(tc_core, test_ng_move);
    tcase_add_test(tc_core, test_ng_set_dir);
    tcase_add_test(tc_core, test_ng_get_dir);
    tcase_add_test(tc_core, test_ng_set_position);
    tcase_add_test(tc_core, test_ng_set_x);
    tcase_add_test(tc_core, test_ng_set_y);
    tcase_add_test(tc_core, test_ng_get_speed_x);
    tcase_add_test(tc_core, test_ng_get_speed_y);
    tcase_add_test(tc_core, test_ng_set_speed_x);
    tcase_add_test(tc_core, test_ng_set_speed_y);
    tcase_add_test(tc_core, test_ng_set_speed);
    tcase_add_test(tc_core, test_ng_get_x);
    tcase_add_test(tc_core, test_ng_get_y);
    tcase_add_test(tc_core, test_ng_get_width);
    tcase_add_test(tc_core, test_ng_get_height);
    tcase_add_test(tc_core, test_ng_set_visible);
    tcase_add_test(tc_core, test_ng_is_entity_by_name);
    tcase_add_test(tc_core, test_ng_check_collision);
    tcase_add_test(tc_core, test_ng_timer_expired);
    tcase_add_test(tc_core, test_ng_set_timer);
    tcase_add_test(tc_core, test_ng_print_centered);
    tcase_add_test(tc_core, test_ng_update_menu);
    tcase_add_test(tc_core, test_ng_create_menu);
    tcase_add_test(tc_core, test_ng_create_popup_menu);
    tcase_add_test(tc_core, test_ng_get_input);
    tcase_add_test(tc_core, test_ng_unregister_from_input);
    tcase_add_test(tc_core, test_ng_register_to_input);
    tcase_add_test(tc_core, test_ng_install_input);
    tcase_add_test(tc_core, test_ng_entity_find);
    tcase_add_test(tc_core, test_ng_add_entity);
    suite_add_tcase(s, tc_core);

    return s;
}

int main()
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = ngames_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
