#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ncurses.h>
#include <panel.h>
#include <Imlib2.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include "lib/ngames.h"
#include <signal.h>

#define PLAYERS 2
#define MAX_POINTS 10
#define DEFAULT_BUF_SIZE 256

static void action_exit(struct ng_entity *ent);
static void action_play(struct ng_entity *ent);
static void update_ball(struct ng_entity *self, long int delta);
static void update_game(struct ng_entity *self, long int delta);
static void control_ai(struct ng_entity *self, long int delta);
static void control_player(struct ng_entity *self, long int delta);

static int maxx;
static int maxy;

int score[PLAYERS];

typedef enum {
    C_UNKNOWN,
    C_FROM_LEFT,
    C_FROM_RIGHT,
    C_FROM_TOP,
    C_FROM_BOTTOM
} collision_dir_t;

typedef enum {
    S_UPPER,
    S_MIDDLE,
    S_LOWER
} sector_part_t;

static bool connected; // temporary
static bool isHost; // temp
static bool isNetwork; // temp
static char ip_text[DEFAULT_BUF_SIZE];
static char port_text[DEFAULT_BUF_SIZE];
static char name[DEFAULT_BUF_SIZE];
static char reason[DEFAULT_BUF_SIZE];
//static char network_buf[DEFAULT_BUF_SIZE];

static void popup_update(struct ng_entity *self, long int delta)
{
    ng_update_menu(self);
}

static void action_unpause(struct ng_entity *ent)
{
    struct ng_entity *block_left, *ball, *block_right, *game;

    ng_delete_entity(ent);

    block_left = ng_entity_find("block_left");
    ng_set_update_cb(block_left, control_ai);
    ball = ng_entity_find("ball");
    ng_set_update_cb(ball, update_ball);
    block_right = ng_entity_find("block_right");
    ng_set_update_cb(block_right, control_player);
    game = ng_entity_find("game");
    ng_set_update_cb(game, update_game);
}

static void action_pause()
{
    struct ng_entity *block_left, *ball, *block_right, *game;

    block_left = ng_entity_find("block_left");
    ng_set_update_cb(block_left, NULL);
    ball = ng_entity_find("ball");
    ng_set_update_cb(ball, NULL);
    block_right = ng_entity_find("block_right");
    ng_set_update_cb(block_right, NULL);
    game = ng_entity_find("game");
    ng_set_update_cb(game, NULL);
}

static void update_game(struct ng_entity *self, long int delta)
{
    struct ng_entity *pause, *exit, *gameover;
    static struct ng_menu_choices pause_choice = {"OK", action_unpause};
    static struct ng_menu_choices exit_choice[2] = {
        {"Yes", action_exit},
        {"No", action_unpause}
    };

    static struct ng_menu_choices gameover_choice[2] = {
        {"Replay", action_play},
        {"Exit", action_exit}
    };

    if (score[0] == MAX_POINTS || score[1] == MAX_POINTS) {
        action_pause();
        gameover = ng_add_entity(T_WINDOW, "gameover");
        if (gameover) {
            ng_add_window(gameover, maxx / 2 - 20, maxy / 2 - 4, 35, 6);
            ng_register_to_input(gameover, I_KEYBOARD);
            ng_create_popup_menu(gameover, gameover_choice, 2, "GAME OVER!");
            ng_set_update_cb(gameover, popup_update);
        } else {
            ng_log("Cannot gameover exit entity\n");
            action_exit(NULL);
        }
    }

    switch(ng_get_input(self)) {
    case 'p':
        action_pause();
        pause = ng_add_entity(T_WINDOW, "pause");
        if (pause) {
            ng_add_window(pause, maxx / 2 - 20, maxy / 2 - 4, 30, 6);
            ng_register_to_input(pause, I_KEYBOARD);
            ng_create_popup_menu(pause, &pause_choice, 1, "Press enter to continue");
            ng_set_update_cb(pause, popup_update);
        } else {
            ng_log("Cannot create pause entity\n");
            action_exit(NULL);
        }
        break;
    case 'q':
        action_pause();
        exit = ng_add_entity(T_WINDOW, "exit");
        if (exit) {
            ng_add_window(exit, maxx / 2 - 20, maxy / 2 - 4, 30, 6);
            ng_register_to_input(exit, I_KEYBOARD);
            ng_create_popup_menu(exit, exit_choice, 2, "Do you really want to exit?");
            ng_set_update_cb(exit, popup_update);
        } else {
            ng_log("Cannot create exit entity\n");
            action_exit(NULL);
        }
        break;
    }
}

static sector_part_t check_block_sector(struct ng_entity *block, struct ng_entity *ball)
{
    if ((ng_get_y(block) + 2) >= ng_get_y(ball)) {
        return S_UPPER;
    }

    if ((ng_get_y(block) + 5) >= ng_get_y(ball)) {
        return S_MIDDLE;
    }

    return S_LOWER;
}

static collision_dir_t check_collision_dir(struct ng_entity *a, struct ng_entity *b)
{
    if (ng_get_x(a) < ng_get_x(b)) {
        return C_FROM_RIGHT;
    }

    if (ng_get_x(a) + ng_get_width(a) > ng_get_x(b) + ng_get_width(b)) {
        return C_FROM_LEFT;
    }

    if (ng_get_y(a) < ng_get_y(b)) {
        return C_FROM_BOTTOM;
    }

    if (ng_get_y(a) + ng_get_height(a) > ng_get_y(b) + ng_get_height(b)) {
        return C_FROM_TOP;
    }

    return C_UNKNOWN;
}

static void update_ball(struct ng_entity *self, long int delta)
{
    int delay = 15;
    struct ng_entity *ent;
    static bool wait = false;

    if (ng_get_dir(self) >= NG_UP_LEFT) {
        delay = 25;
    }

    if (wait) {
        delay = 500;
    }

    if (!ng_timer_expired(self, delta)) {
        return;
    }

    ng_move(self);
    ng_set_timer(self, delay);

    wait = false;

    ent = ng_check_collision(self);

    if (!ent) {
        return;
    }

    ng_beep();

    if (ng_is_entity_by_name(ent, "border")) {
        switch (check_collision_dir(self, ent)) {
        case C_FROM_RIGHT:
            score[1]++;
            ng_set_position(self, maxx / 2, maxy / 2);
            ng_set_speed(self, 1, 1);
            ng_set_dir(self, NG_RIGHT);
            wait = true;
            break;
        case C_FROM_LEFT:
            score[0]++;
            ng_set_position(self, maxx / 2, maxy / 2);
            ng_set_speed(self, 1, 1);
            ng_set_dir(self, NG_LEFT);
            wait = true;
            break;
        case C_FROM_BOTTOM:
            if (ng_get_dir(self) == NG_UP_RIGHT) {
                ng_set_dir(self, NG_DOWN_RIGHT);
            } else {
                ng_set_dir(self, NG_DOWN_LEFT);
            }
            ng_set_speed(self, 2, 1);
            break;
         case C_FROM_TOP:
            if (ng_get_dir(self) == NG_DOWN_RIGHT) {
                ng_set_dir(self, NG_UP_RIGHT);
            } else {
                ng_set_dir(self, NG_UP_LEFT);
            }
            ng_set_speed(self, 2, 1);
            break;
         default:
            break;
         }
    } else if (ng_is_entity_by_name(ent, "block_right")) {
        switch (check_block_sector(ent, self)) {
        case S_UPPER:
             ng_set_dir(self, NG_UP_LEFT);
             ng_set_speed(self, 2, 1);
             break;
        case S_MIDDLE:
             ng_set_dir(self, NG_LEFT);
             ng_set_speed(self, 1, 1);
             break;
        case S_LOWER:
             ng_set_dir(self, NG_DOWN_LEFT);
             ng_set_speed(self, 2, 1);
             break;
        }
    } else if (ng_is_entity_by_name(ent, "block_left")) {
        switch (check_block_sector(ent, self)) {
        case S_UPPER:
            ng_set_dir(self, NG_UP_RIGHT);
            ng_set_speed(self, 2, 1);
            break;
        case S_MIDDLE:
            ng_set_dir(self, NG_RIGHT);
            ng_set_speed(self, 1, 1);
            break;
        case S_LOWER:
            ng_set_dir(self, NG_DOWN_RIGHT);
            ng_set_speed(self, 2, 1);
            break;
        }
    }
}

static void control_block_network(struct ng_entity *self, long int delta)
{
    struct ng_entity *ent;
    int old_y = ng_get_y(self);

    switch (ng_get_input(self)) {
    case KEY_UP:
        ng_set_dir(self, NG_UP);
        ng_move(self);
        break;
    case KEY_DOWN:
        ng_set_dir(self, NG_DOWN);
        ng_move(self);
        break;
    }

    ent = ng_check_collision(self);

    if (ent) {
        if (ng_is_entity_by_name(ent, "border")) {
            ng_set_y(self, old_y);
        }
    }
}

static void control_player(struct ng_entity *self, long int delta)
{
    struct ng_entity *ent;
    int old_y = ng_get_y(self);
    int keycode = ng_get_input(self);

    switch (keycode) {
    case KEY_UP:
        ng_set_dir(self, NG_UP);
        ng_move(self);
        break;
    case KEY_DOWN:
        ng_set_dir(self, NG_DOWN);
        ng_move(self);
        break;
    }

    ent = ng_check_collision(self);

    if (ent) {
        if (ng_is_entity_by_name(ent, "border")) {
            ng_set_y(self, old_y);
        }
    }

    if (connected) {
        ng_send_keycode(keycode);
    } else if (isHost) {
        ng_send_keycode_from_host(keycode);
    }
}

static void control_ai(struct ng_entity *self, long int delta)
{
    int r;
    struct ng_entity *border;
    int old_y = ng_get_y(self);

    if (!ng_timer_expired(self, delta)) {
        return;
    }

    ng_set_timer(self, 50);

    srand(time(NULL));
    r = rand();
    if (r > (RAND_MAX / 2)) {
        ng_set_dir(self, NG_UP);
    } else {
        ng_set_dir(self, NG_DOWN);
    }
    
    ng_move(self);

    border = ng_check_collision(self);

    if (border) {
        if (ng_is_entity_by_name(border, "border")) {
            ng_set_y(self, old_y);
        }
    }
}

static void update_status(struct ng_entity *ent, long int delta)
{
    char status_text[DEFAULT_BUF_SIZE];

    snprintf(status_text, sizeof(status_text), "Player 1  %d : %d  Player 2",
        score[0], score[1]);

    ng_print_centered(ent, ng_get_height(ent) - 2, status_text);
}

static void setup_gameplay()
{
    struct ng_entity *status, *game, *border, *ball, *block_left, *block_right;

    status = ng_add_entity(T_WINDOW, "status");
    if (status) {
        ng_add_window(status, 1, 1, maxx - 2, maxy / 10);
        ng_load_image(status, "res/status.jpg");
        ng_set_update_cb(status, update_status);
    } else {
        ng_log("Something wrong. Shouldn't happen.\n");
        action_exit(NULL);
    }

    game = ng_add_entity(T_ABSTRACT, "game");
    if (game) {
        ng_register_to_input(game, I_KEYBOARD);
        ng_set_update_cb(game, update_game);
    } else {
        ng_log("Cannot create game entity\n");
        action_exit(NULL);
    }

    border = ng_add_entity(T_BORDER, "border");
    if (border) {
        ng_add_window(border, 1, maxy / 10 + 1, maxx-2, maxy - (maxy / 10) - 2);
    } else {
        ng_log("Cannot create border entity\n");
        action_exit(NULL);
    }

    ball = ng_add_entity(T_ACTOR, "ball");
    if (ball) {
        ng_add_window(ball, maxx / 2, maxy / 2, 2, 2);
        ng_set_dir(ball, NG_RIGHT);
        ng_fill_window(ball, '*');
        ng_set_update_cb(ball, update_ball);
        ng_set_timer(ball, 25);
    } else {
        ng_log("Cannot create ball entity\n");
        action_exit(NULL);
    }

    block_left = ng_add_entity(T_ACTOR, "block_left");
    if (block_left) {
        ng_add_window(block_left, 10, maxy / 2, 4, 10);
        ng_fill_window(block_left, '*');
        if (!isNetwork) {
            ng_set_update_cb(block_left, control_ai);
        } else if(isHost) {
            ng_set_update_cb(block_left, control_player);
            ng_register_to_input(block_left, I_KEYBOARD);
            ng_log("host: block_left keyboard\n");
        } else {
            ng_set_update_cb(block_left, control_block_network);
            ng_register_to_input(block_left, I_NETWORK);
            ng_log("client: block_left network\n");
        }
        ng_set_timer(block_left, 100);
    } else {
        ng_log("Cannot create left block entity\n");
        action_exit(NULL);
    }

    block_right = ng_add_entity(T_ACTOR, "block_right");
    if (block_right) {
        ng_add_window(block_right, maxx - 10, maxy / 2, 4, 10);
        ng_fill_window(block_right, '*');
        if (!isNetwork || !isHost) {
            ng_set_update_cb(block_right, control_player);
            ng_register_to_input(block_right, I_KEYBOARD);
            ng_log("client: block_right keyboard\n");
        } else if (isHost) {
            ng_set_update_cb(block_right, control_block_network);
            ng_register_to_input(block_right, I_NETWORK);
            ng_log("host: block_right network\n");
        }
    }

    ng_log("Setup completed\n");
}

static void reset_gameplay() {
    int i;
    
    for (i = 0; i < PLAYERS; i++) {
        score[i] = 0;
    }
}

static void update_menu(struct ng_entity *ent, long int delta)
{
/*    if (connected && ng_timer_expired(ent, delta)) {
        if (!ng_rcv_msg(network_buf, DEFAULT_BUF_SIZE)) {
            ng_log("Client: %s\n", network_buf);
        }
        ng_send_msg("Hello");
        ng_set_timer(ent, 1000);
    }*/
    ng_update_menu(ent);
}

static void update_form(struct ng_entity *ent, long int delta)
{
    ng_update_form(ent);
}

static void settings_on_quit(struct ng_entity *ent)
{
    ng_delete_entity(ent);
    ng_register_to_input(ng_entity_find("menu"), I_KEYBOARD);
}

static void settings_fetch(struct ng_entity *ent)
{
    int i;

    ng_delete_entity(ent);
    ng_register_to_input(ng_entity_find("menu"), I_KEYBOARD);

    for (i = 0; i < DEFAULT_BUF_SIZE; i++) {
        if (name[i] == ' ') {
            name[i] = '\0';
        }
        if (reason[i] == ' ') {
            reason[i] = '\0';
        }
    }

    ng_log("name = %s, reason = %s\n", name, reason);
}

static void action_settings(struct ng_entity *ent)
{
    static struct ng_form_data form_data[2] = {
        {"Name: ", 12, name},
        {"Why:", 18, reason},
    };
    static struct ng_form_ops ops = {
        .fetch = settings_fetch,
        .quit = settings_on_quit,
    };
    struct ng_entity *settings = ng_add_entity(T_WINDOW, "settings");

    if (!settings) {
        ng_log("Cannot create settings entity\n");
        action_exit(NULL);
    }

    ng_add_window(settings, maxx / 2 - 20, maxy / 2 - 4, 35, 6);
    ng_register_to_input(settings, I_KEYBOARD);
    ng_create_form(settings, form_data, 2, &ops);
    ng_set_update_cb(settings, update_form);

    ng_unregister_from_input(ng_entity_find("menu"));

}

static void update_wait(struct ng_entity *self, long int delta)
{
    if (ng_timer_expired(self, delta)) {
        ng_delete_entity(self);
        ng_register_to_input(ng_entity_find("network_popup"), I_KEYBOARD);
        return;
    }
}

static void action_host(struct ng_entity *ent)
{
    int ret;
    int timeout = 60;
    char wait_text[DEFAULT_BUF_SIZE];
    struct ng_entity *network_popup = ng_entity_find("network_popup");
    struct ng_entity *wait_for_client = ng_add_entity(T_WINDOW, "wait_for_client");

    if (!wait_for_client) {
        ng_log("Cannot create wait_for_client entity\n");
        action_exit(NULL);
    }

    ng_unregister_from_input(network_popup);
    ng_add_window(wait_for_client, maxx / 2 - 20, maxy / 2 - 4, 35, 6);
    snprintf(wait_text, sizeof(wait_text), "Waiting %d seconds...", timeout);
    ng_print_centered(wait_for_client, 2, wait_text);
    ng_set_update_cb(wait_for_client, update_wait);

    ng_refresh();

    /* This call is the only one blocking */
    ret = ng_host(1234, timeout);
    if (ret == -1) {
        ng_log("Error during establishing a server\n");
        action_exit(NULL);
    }

    if (ret == 1) {
        ng_print_centered(wait_for_client, 3, "Timeout!");
        ng_set_timer(wait_for_client, 1000);
        return;
    }

    ng_delete_entity(wait_for_client);
    ng_delete_entity(network_popup);
    ng_register_to_input(ng_entity_find("menu"), I_KEYBOARD);
    ng_log("Client connected\n");
    isNetwork = true;
    isHost = true;
    action_play(ent);
}

static void network_join_on_quit(struct ng_entity *ent)
{
    ng_delete_entity(ent);
    ng_register_to_input(ng_entity_find("menu"), I_KEYBOARD);
}

static void network_join_fetch(struct ng_entity *ent)
{
    int i;

    ng_delete_entity(ent);
    ng_register_to_input(ng_entity_find("menu"), I_KEYBOARD);

    for (i = 0; i < DEFAULT_BUF_SIZE; i++) {
        if (ip_text[i] == ' ') {
            ip_text[i] = '\0';
        }
    }
    if (ng_connect(ip_text, atoi(port_text)) != -1) {
        connected = true;
        isNetwork = true;
        isHost = false;
        action_play(ent);
    }
}

static void action_network_join(struct ng_entity *ent)
{
    static struct ng_form_data form_data[2] = {
        {"IP: ", 20, ip_text},
        {"Port: ", 20, port_text}
    };

    static struct ng_form_ops ops = {
        .fetch = network_join_fetch,
        .quit = network_join_on_quit,
    };

    struct ng_entity *get_ip = ng_add_entity(T_WINDOW, "get_ip");

    if (!get_ip) {
        ng_log("Cannot create get_ip entity\n");
        action_exit(NULL);
    }
    struct ng_entity *network_popup = ng_entity_find("network_popup");

    if (network_popup) {
        ng_delete_entity(network_popup);
    } else {
        ng_log("Cannot get network_popup in action_host\n");
        action_exit(NULL);
    }

    ng_add_window(get_ip, maxx / 2 - 20, maxy / 2 - 4, 35, 6);
    ng_register_to_input(get_ip, I_KEYBOARD);
    ng_create_form(get_ip, form_data, 2, &ops);
    ng_set_update_cb(get_ip, update_form);
}

static void action_cancel(struct ng_entity *ent)
{
    ng_delete_entity(ent);
    ng_register_to_input(ng_entity_find("menu"), I_KEYBOARD);
}

static void action_network(struct ng_entity *ent)
{
    static struct ng_menu_choices network_choice[3] = {
        {"Host", action_host},
        {"Join", action_network_join},
        {"Cancel", action_cancel}
    };
    struct ng_entity *network_popup = ng_add_entity(T_WINDOW, "network_popup");

    if (network_popup) {
        ng_add_window(network_popup, maxx / 2 - 20, maxy / 2 - 4, 35, 6);
        ng_register_to_input(network_popup, I_KEYBOARD);
        ng_unregister_from_input(ng_entity_find("menu"));
        ng_create_popup_menu(network_popup, network_choice, 3, "Select mode");
        ng_set_update_cb(network_popup, popup_update);
    } else {
        ng_log("Cannot create network_popup entity\n");
        action_exit(NULL);
    }
}

static void action_exit(struct ng_entity *ent)
{
   ng_finish();
   exit(EXIT_SUCCESS);
}

static void action_play(struct ng_entity *ent)
{
    ng_delete_all_entities();

    reset_gameplay();
    setup_gameplay();
}

static void start_menu()
{
    struct ng_entity *logo, *menu;
    struct ng_entity *background;

    static struct ng_menu_choices choices[] = {
        {"PLAY", action_play},
        {"NETWORK", action_network},
        {"SETTINGS", action_settings},
        {"EXIT", action_exit}
    };


    background = ng_add_entity(T_WINDOW, "background");
    ng_add_window(background, 0, 0, maxx, maxy);
    ng_load_image(background, "res/pong1.jpg");

    logo = ng_add_entity(T_WINDOW, "logo");
    if (!logo) {
        ng_log("Cannot add logo. It's bad.\n");
        action_exit(NULL);
    }

    ng_add_window(logo, 1, 1, maxx - 2,  maxy / 4);
    ng_load_image(logo, "res/logo.jpg");

    menu = ng_add_entity(T_WINDOW, "menu");
    if (menu) {
        ng_register_to_input(menu, I_KEYBOARD);
        ng_add_window(menu, maxx / 2 - maxx/16, maxy / 2 - maxy /6, maxx / 8, maxy / 3);
        ng_set_update_cb(menu, update_menu);
        ng_create_menu(menu, choices, sizeof(choices) / sizeof(choices[0]));
//        ng_set_timer(menu, 1000);
    } else {
        ng_log("Cannot create menu\n");
        action_exit(NULL);
    }
}

int main()
{
    if (ng_init() == -1) {
        return 1;
    }
    
    maxx = ng_get_terminal_width();
    maxy = ng_get_terminal_height();

    start_menu();
    
    ng_run();

    return 0;
}
